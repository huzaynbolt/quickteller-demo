﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using alfred.service.Payments.Quickteller;
using Interswitch;


namespace alfred.webapi.quickteller.Controllers
{
    [System.Web.Http.Route("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly IInquireTransaction _inquireTransaction;
        private readonly IQuickTellerPaymentTransaction _paymentTransaction;
        private readonly IQueryTransaction _queryTransaction;
        public PaymentController(IInquireTransaction inquireTransaction,IQuickTellerPaymentTransaction paymentTransaction, IQueryTransaction queryTransaction)
        {
            _inquireTransaction = inquireTransaction;
            _paymentTransaction = paymentTransaction;
            _queryTransaction = queryTransaction;
        }



        [Route("interswitch/{id}")]
        public IHttpActionResult Post(string id)
        {

            var interswitch = new Interswitch.Interswitch("IKIAB2BB4C5AD487B48522BEE2E87B65BF422FC12E55", "IKIAB2BB4C5AD487B48522BEE2E87B65BF422FC12E55", "dev");
            Interswitch.Config config = new Config(
                "GET", "https://sandbox.interswitchng.com/api/v2/quickteller/transactions/huzaynbolt@gmail.com/inquiry", "IKIAB2BB4C5AD487B48522BEE2E87B65BF422FC12E55", "IKIAB2BB4C5AD487B48522BEE2E87B65BF422FC12E55", "IKIAB2BB4C5AD487B48522BEE2E87B65BF422FC12E55", null);
            var resp = config.GetNonce() + " " + id;

            return Ok(resp);
        }

        [Route("inquiry")]
        public IHttpActionResult Post(InquiryData data)
        {

            var resp = _inquireTransaction.Inquire(data); 
         
            return Ok(resp);
        }

        // POST api/values
        [Route("send")]
        public IHttpActionResult Post(UserTransactionData data)
        {
            var resp = _paymentTransaction.SendTransaction(data);
            return Ok(resp);
        }

        // PUT api/values/5
        [Route("send_token/{token}")]
        public IHttpActionResult PostToken(UserTransactionData transactionData, string token)
        {
            var resp = _paymentTransaction.SendTransactionWithOneTimeToken(transactionData, token);
            return Ok(resp);
        }

        [Route("query")]
        public IHttpActionResult QueryTransaction(QuickTellerTransactionData transactionData)
        {
            var resp = _queryTransaction.Query(transactionData);
            return Ok(resp);
        }

        // DELETE api/values/5
        [Route("{id}")]
        public void Delete(int id)
        {
        }
    }
}
