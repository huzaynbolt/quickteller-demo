﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Http;
using alfred.service.Logging;
using alfred.service.Payments.Quickteller;
using alfred.service.RestClient;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Ninject.Web.WebApi;



[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(alfred.webapi.quickteller.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(alfred.webapi.quickteller.NinjectWebCommon), "Stop")]
namespace alfred.webapi.quickteller
{
    public static class NinjectWebCommon
    {

        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;

            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {

            string terminalId = ConfigurationManager.AppSettings["TerminalId"];

            kernel.Bind<RequestHeadersUtil>().To<RequestHeadersUtil>().InRequestScope().WithConstructorArgument("contentType", "application/json")
                .WithConstructorArgument("terminalId", terminalId);

            kernel.Bind<IServiceConsumer>().To<ServiceConsumer>().InRequestScope()
                .WithConstructorArgument("logger", default(ILogger));

            var serviceConsumer = kernel.Get<IServiceConsumer>();
            var requestHeader = kernel.Get<RequestHeadersUtil>();


            string clientId = ConfigurationManager.AppSettings["ClientId"];

            string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];


            string accessToken = ConfigurationManager.AppSettings["AccessToken"];


            var authCredentials = new InterSwitchOauthCredentials() { AccessToken = accessToken, ClientId = clientId, SecretKey = clientSecret };


            kernel.Bind<IQuickTellerPaymentTransaction>().To<QuickTellerPaymentTransaction>().InRequestScope()
                .WithConstructorArgument("serviceConsumer", serviceConsumer)
                .WithConstructorArgument("requestHeadersUtil", requestHeader)
                .WithConstructorArgument("credentials", authCredentials);



            kernel.Bind<IInquireTransaction>().To<InquireTransaction>().InRequestScope()
                .WithConstructorArgument("serviceConsumer", serviceConsumer)
                .WithConstructorArgument("requestHeadersUtil", requestHeader)
                .WithConstructorArgument("switchOauthCredentials", authCredentials);


            kernel.Bind<IQueryTransaction>().To<QueryTransaction>().InRequestScope()
                .WithConstructorArgument("serviceConsumer", serviceConsumer)
                .WithConstructorArgument("requestHeadersUtil", requestHeader)
                .WithConstructorArgument("credentials", authCredentials);


          

        }

    }

}
