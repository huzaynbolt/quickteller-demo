﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Extensions;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Caching;
using alfred.service.Helpers;

namespace alfred.service.Customers
{
    public class CustomerAttributeService:ICustomerAttributeService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : entity ID
        /// {1} : key group
        /// </remarks>
        // ReSharper disable once InconsistentNaming
        private const string GENERICATTRIBUTE_KEY = "alfred.genericattribute.{0}-{1}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private const string GENERICATTRIBUTE_PATTERN_KEY = "alfred.genericattribute.";
        #endregion

        #region Fields

        private readonly ICustomerAttributeRepository _genericAttributeRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IUnitOfWork _unitOfWork;   

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="genericAttributeRepository">Generic attribute repository</param>
        /// <param name="unitOfWork"></param>
        public CustomerAttributeService(ICacheManager cacheManager,
            ICustomerAttributeRepository genericAttributeRepository, IUnitOfWork unitOfWork)
        {
            _cacheManager = cacheManager;
            _genericAttributeRepository = genericAttributeRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes an attribute
        /// </summary>
        /// <param name="attribute">Attribute</param>
        public virtual void DeleteAttribute(CustomerAttribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            _genericAttributeRepository.Delete(attribute);

            //cache
            _cacheManager.RemoveByPattern(GENERICATTRIBUTE_PATTERN_KEY);
            Save();
            
        }

        /// <summary>
        /// Gets an attribute
        /// </summary>
        /// <param name="attributeId">Attribute identifier</param>
        /// <returns>An attribute</returns>
        public virtual CustomerAttribute GetAttributeById(long attributeId)
        {
            if (attributeId == 0)
                return null;

            return _genericAttributeRepository.GetById(attributeId);
        }

        /// <summary>
        /// Inserts an attribute
        /// </summary>
        /// <param name="attribute">attribute</param>
        public virtual void InsertAttribute(CustomerAttribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            _genericAttributeRepository.Add(attribute);
            Save();

            //cache
            _cacheManager.RemoveByPattern(GENERICATTRIBUTE_PATTERN_KEY);

        }

        /// <summary>
        /// Updates the attribute
        /// </summary>
        /// <param name="attribute">Attribute</param>
        public virtual void UpdateAttribute(CustomerAttribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            _genericAttributeRepository.UpdateNoTracking(attribute);
            Save();

            //cache
            _cacheManager.RemoveByPattern(GENERICATTRIBUTE_PATTERN_KEY);

        }

        /// <summary>
        /// Get attributes
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="keyGroup">Key group</param>
        /// <returns>Get attributes</returns>
        public virtual IList<CustomerAttribute> GetAttributesForEntity(long entityId, string keyGroup)
        {
            string key = string.Format(GENERICATTRIBUTE_KEY, entityId, keyGroup);
            return _cacheManager.Get(key, () =>
            {
                var query = from ga in _genericAttributeRepository.GetAll()
                            where ga.EntityId == entityId &&
                            ga.KeyGroup == keyGroup
                            select ga;
                var attributes = query.ToList();
                return attributes;
            });
        }

        public IPagedList<CustomerAttribute> GetAttributesForEntity(long entityId, string keyGroup, int pageIndex, int pageSize = Int32.MaxValue)
        {
            string key = string.Format(GENERICATTRIBUTE_KEY + "{2}-{3}", entityId, keyGroup, pageIndex, pageSize ) ;

            var results = _cacheManager.Get(key, () =>
            {
                var query = from ga in _genericAttributeRepository.GetAll()
                    where ga.EntityId == entityId &&
                          ga.KeyGroup == keyGroup
                    select ga;
                return query;
            }); 
            results = results.OrderByDescending(c => c.Id);
            return  new PagedList<CustomerAttribute>(results.AsQueryable(), pageIndex, pageSize);
        }

        /// <summary>
        /// Save attribute value
        /// </summary>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public virtual void SaveAttribute<TPropType>(BaseEntity entity, string key, TPropType value)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (key == null)
                throw new ArgumentNullException("key");

            string keyGroup = entity.GetUnproxiedEntityType().Name;

            var props = GetAttributesForEntity(entity.Id, keyGroup)
                .ToList();
            var prop = props.FirstOrDefault(ga =>
                ga.Key.Equals(key, StringComparison.InvariantCultureIgnoreCase)); //should be culture invariant

            var valueStr = CommonHelper.To<string>(value);

            if (prop != null)
            {
                if (string.IsNullOrWhiteSpace(valueStr))
                {
                    //delete
                    DeleteAttribute(prop);
                }
                else
                {
                    //update
                    prop.Value = valueStr;
                    UpdateAttribute(prop);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(valueStr))
                {
                    //insert
                    prop = new CustomerAttribute
                    {
                        EntityId = entity.Id,
                        Key = key,
                        KeyGroup = keyGroup,
                        Value = valueStr,

                    };
                    InsertAttribute(prop);
                }
            }
        }

        public void DeleteAttribute(IList<CustomerAttribute> attributes)
        {
            if (attributes == null)
                throw new ArgumentNullException("attributes");

            _genericAttributeRepository.Delete(attributes);

            //cache
            _cacheManager.RemoveByPattern(GENERICATTRIBUTE_PATTERN_KEY);
            Save();
        }

        public List<CustomerAttribute> GetAttributes(long organizationId)
        {
            string key = string.Format("CUSTOMER-ATTRIBUTE-{0}" , organizationId  ) ;

            return _cacheManager.Get(key, () =>
            {

                var query = _genericAttributeRepository.GetAll();
                if (organizationId > 0)
                {
                    
                }

                return query.ToList();
            });
        }

        public void ClearCache()
        {
            //cache
            _cacheManager.RemoveByPattern(GENERICATTRIBUTE_PATTERN_KEY);
        }

        private void Save()
        {
            _unitOfWork.Commit();
        }
        #endregion
    }
}