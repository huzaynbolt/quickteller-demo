﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Caching;

// ReSharper disable InconsistentNaming

namespace alfred.service.Customers
{
    public class CustomerService : ICustomerService
    {
        private readonly ICacheManager _cacheManager;
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(ICacheManager cacheManager, ICustomerRepository customerRepository,
            IUnitOfWork unitOfWork)
        {
            _cacheManager = cacheManager;
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }

        // ReSharper disable once InconsistentNaming
        private const string CUSTOMER_ID_KEY = "alfred.customer-{0}";
        private  const string CUSTOMER_ALL_KEY = "alfred.customer.all";
        public IPagedList<Customer> Search(int institutionId = 0, DateTime? createdOnFrom = null, DateTime? createdOnTo = null,
           string customerId = null, string phone = null, string firstName = null, string lastName = null, string email = null, int pageIndex = 0,
            int pageSize = Int32.MaxValue)
        {
            
            var key = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", CUSTOMER_ALL_KEY, institutionId, createdOnFrom, createdOnTo, customerId,phone, firstName,lastName,email );

            var results = _cacheManager.Get(key, () =>
            {
                var query = _customerRepository.GetAll();
                if (institutionId > 0)
                {
                    query = query.Where(x => x.OrganizationId == institutionId);
                }
                if (createdOnFrom.HasValue)
                    query = query.Where(x => createdOnFrom.Value <= x.AnchorTime);

                if (createdOnTo.HasValue)
                    query = query.Where(x => createdOnTo.Value >= x.AnchorTime);

                if (!string.IsNullOrWhiteSpace(customerId))
                {
                    query = query.Where(x => x.CustomerId.Contains(customerId));

                }
                if (!string.IsNullOrWhiteSpace(phone))
                {
                    query = query.Where(x => x.PhoneNumber.Contains(phone));

                }
                if (!string.IsNullOrWhiteSpace(firstName))
                {
                    query = query.Where(x => x.FirstName.Contains(firstName));
                }
                if (!string.IsNullOrWhiteSpace(lastName))
                {
                    query = query.Where(x => x.LastName.Contains(lastName));
                }

                if (!string.IsNullOrWhiteSpace(email))
                {
                    query = query.Where(x => x.Email.Contains(email));

                }

                return query;
            });
            results = results.OrderByDescending(c => c.AnchorTime);
            return new PagedResult<Customer>(results.AsQueryable(), pageIndex, pageSize);
        }

        public void Insert(Customer customer)
        {
            _customerRepository.Add(customer);
            _unitOfWork.Commit();
            _cacheManager.RemoveByPattern(CUSTOMER_ALL_KEY);
        }

        public Customer GetCachedById(long id)
        {
            var key = string.Format(CUSTOMER_ID_KEY, id);
            if (id <= 0)
                return null;
           return _cacheManager.Get(key, () => _customerRepository.GetById(id));
        }

        public Customer GetById(string id)
        {
            var key = string.Format(CUSTOMER_ID_KEY, id);
            if (string.IsNullOrEmpty(id))
                return null;
            return _cacheManager.Get(key, () => _customerRepository.Get(x=>x.PhoneNumber == id));
        }

        public Customer GetById(long id)
        {
            
            if (id <= 0)
                return null;
            return _customerRepository.GetById(id);
        }

        public void Update(Customer customer)
        {
            _customerRepository.Update(customer);
            _unitOfWork.Commit();
            _cacheManager.RemoveByPattern(CUSTOMER_ALL_KEY);
        }

        public void Delete(IList<Customer> customers)
        {
            _customerRepository.Delete(customers);
            _unitOfWork.Commit();
            _cacheManager.RemoveByPattern(CUSTOMER_ALL_KEY);
        }

        public void Delete(Customer customer)
        {
            _customerRepository.Delete(customer);
            _unitOfWork.Commit();
            _cacheManager.RemoveByPattern(CUSTOMER_ALL_KEY);
        }

        public IQueryable<Customer> GetAll()
        {
            return _customerRepository.GetAll();
            
        }

        public Customer GetByIdAndInstitution(string id, long institution)
        {
            var key = string.Format(CUSTOMER_ID_KEY, id+institution);
            if (string.IsNullOrEmpty(id) || institution <= 0)
                return null;
            return _cacheManager.Get(key, () => _customerRepository.Get(x => x.CustomerId == id &&  x.OrganizationId == institution));
        }
    }
}