﻿namespace alfred.service.Customers
{
    public class CustomerAttributeNames
    {
        public static string TopUpAmount { get { return "TopUpAmount"; } }

        public static string CustomerBalancePercent​ { get { return "CustomerBalancePercent​"; } }

        public static string CustomerBalance { get { return "CustomerBalance"; } }

        public static string RecurringStartDate { get { return "RecurringStartDate"; } }

        public static string RecurringCyclePeriod { get { return "RecurringCyclePeriod"; } }

        public static string ChargeToken { get { return "ChargeToken"; } }
        public static string IsSignedUp { get { return "IsSignedUp"; } }
        public static string NextStatus { get { return "NextStatus"; } }
        public static string Status { get { return "Status"; } }
        public static string PhoneNumber { get { return "PhoneNumber"; } }
        public static string Email { get { return "Email"; } }
        public static string ExpireDate { get { return "ExpireDate"; } }
        public static string PortalFeeNgn { get { return " PortalFeeNGN"; } }
        public static string RecurringInterval { get { return " RecurringInterval"; } }
    }
}