﻿using System.Collections.Generic;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;

namespace alfred.service.Customers
{
    public interface ICustomerAttributeService
    {
        /// <summary>
        /// Deletes an attribute
        /// </summary>
        /// <param name="attribute">Attribute</param>
        void DeleteAttribute(CustomerAttribute attribute);

        /// <summary>
        /// Gets an attribute
        /// </summary>
        /// <param name="attributeId">Attribute identifier</param>
        /// <returns>An attribute</returns>
        CustomerAttribute GetAttributeById(long attributeId);

        /// <summary>
        /// Inserts an attribute
        /// </summary>
        /// <param name="attribute">attribute</param>
        void InsertAttribute(CustomerAttribute attribute);

        /// <summary>
        /// Updates the attribute
        /// </summary>
        /// <param name="attribute">Attribute</param>
        void UpdateAttribute(CustomerAttribute attribute);

        /// <summary>
        /// Get attributes
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="keyGroup">Key group</param>
        /// <returns>Get attributes</returns>
        IList<CustomerAttribute> GetAttributesForEntity(long entityId, string keyGroup);

        /// <summary>
        /// Get attributes
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="keyGroup">Key group</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Get attributes</returns>
        IPagedList<CustomerAttribute> GetAttributesForEntity(long entityId, string keyGroup,  int pageIndex , int pageSize = int.MaxValue);

        /// <summary>
        /// Save attribute value
        /// </summary>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        void SaveAttribute<TPropType>(BaseEntity entity, string key, TPropType value);

        void DeleteAttribute(IList<CustomerAttribute> attributes);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        List<CustomerAttribute> GetAttributes(long organizationId);

        void ClearCache();
    }
}