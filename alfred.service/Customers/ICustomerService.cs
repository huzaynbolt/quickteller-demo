﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;

namespace alfred.service.Customers
{
    public interface ICustomerService
    {
      
       
        IPagedList<Customer>  Search(int institutionId = 0, DateTime? createdOnFrom = null,
            DateTime? createdOnTo = null,
            string customerId = null,
            string phone = null,
            string firstName = null,
            string lastName = null, 
            string email = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        void Insert(Customer customer);
        Customer GetCachedById(long id);
        Customer GetById(string id);
        Customer GetById(long id);
        void Update(Customer customer);
        void Delete(IList<Customer> customers);
        void Delete(Customer customer);
        IQueryable<Customer> GetAll();

        Customer GetByIdAndInstitution(string id, long institution);
    }
}