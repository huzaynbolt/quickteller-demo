﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Text;
using alfred.service.Logging;
using Newtonsoft.Json;

namespace alfred.service.RestClient
{
    public class ServiceConsumer : IServiceConsumer
    {
        private readonly ILogger _logger;

        public ServiceConsumer(ILogger logger)
        {
            _logger = logger;
        }

        public T Get<T>(NameValueCollection headers, NameValueCollection parameters, string serviceName, bool useSsl)
        {

            var _parameters = new List<string>();
            using (var httpClient = new HttpClient())
            {
                httpClient.Timeout = new TimeSpan(2, 60, 60);
                httpClient.MaxResponseContentBufferSize = 2147483647L;

                var endpoint = serviceName;
                if (parameters != null && parameters.Count > 0)
                {
                    endpoint += "?";

                    for (int i = 0; i < parameters.Keys.Count; i++)
                    {
                        _parameters.Add(string.Concat(parameters.Keys[i] + "=", parameters[i]));
                    }
                    endpoint += string.Join("&", _parameters);
                }
                    

                
                _logger.Information(string.Format("Request to endpoint -> {0} ", endpoint), new Exception());
                try
                {
                    for (var i = 0; i < headers.Count; i++)
                    {
                        httpClient.DefaultRequestHeaders.Add(headers.GetKey(i), headers[i]);
                    }
                    if (useSsl)
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    }
                    var response = httpClient.GetAsync(endpoint).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var r = response.Content.ReadAsStringAsync().Result;
                        throw new Exception(string.Format("{0}: {1}", response.StatusCode, r));
                    }
                    var json = response.Content.ReadAsStringAsync().Result;
                    _logger.Information(string.Format("endpoint -> {0} ", endpoint), new Exception(json));

                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (HttpRequestException exception)
                {
                    _logger.Error(exception.Message, exception);
                    throw;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message, exception);
                }
                return default(T);
            }
        }

        public T Get<T>(int id, string endpoint)
        {
            using (var httpClient = NewHttpClient())
            {
                try
                {
                    var response = httpClient.GetAsync(endpoint + id).Result;

                    var json = response.Content.ReadAsStringAsync().Result;
                    _logger.Information(string.Format("endpoint -> {0} ", endpoint), new Exception(json));
                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (HttpRequestException exception)
                {
                    _logger.Error(exception.Message, exception);
                    throw;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message, exception);
                }
                return default(T);
            }
        }

        protected HttpClient NewHttpClient()
        {
            return new HttpClient();
        }


        public T Post<T>(NameValueCollection headers, object data, string endpoint, bool useSsl = true, bool asJson=false)
        {
            var serviceUrl = endpoint;
            using (var httpClient = NewHttpClient())
            {
                try
                {
                    for (var i = 0; i < headers.Count; i++)
                    {
                        string header = headers[i];
                        string key = headers.GetKey(i);
                        httpClient.DefaultRequestHeaders.Add(headers.GetKey(i), headers[i]);
                    }
                    if (useSsl)
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    }

                    string json = "";
                    if (asJson)
                    {
                        var resp = httpClient.PostAsJsonAsync(serviceUrl, data).Result;
                        
                        

                        json = resp.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        var result = httpClient.PostAsync(serviceUrl, new StringContent(
                            data.ToString(),
                            Encoding.UTF8,
                            "application/json")).Result;
                        json = result.Content.ReadAsStringAsync().Result;
                    }
                  

                    
                    
                    //_logger.Information(string.Format("endpoint -> {0} ", endpoint), new Exception(json));
                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (HttpRequestException exception)
                {
                    _logger.Error(exception.Message, exception);
                    throw;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message, exception);
                }
                return default(T);
            }
        }


        public string Delete(int id, string endpoint)
        {
            using (var httpClient = NewHttpClient())
            {
                try
                {
                    var result = httpClient.DeleteAsync(endpoint + id).Result;
                    var json = result.Content.ReadAsStringAsync().Result;
                    _logger.Information(string.Format("endpoint -> {0} ", endpoint), new Exception(json));
                    return json;
                }
                catch (HttpRequestException exception)
                {
                    _logger.Error(exception.Message, exception);
                    throw;
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.Message, exception);
                }
                return string.Empty;
            }
        }
    }
}