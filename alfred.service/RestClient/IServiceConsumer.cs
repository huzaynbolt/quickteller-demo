﻿using System.Collections.Specialized;

namespace alfred.service.RestClient
{
    public interface IServiceConsumer
    {
        T Get<T>(NameValueCollection headers, NameValueCollection parameters, string serviceName,bool useSsl =true);

        string Delete(int id, string endpoint);
        
        T Get<T>(int id, string endpoint);
        T Post<T>(NameValueCollection headers, object data, string endpoint,bool useSsl = true, bool asJson=false);
    }
}