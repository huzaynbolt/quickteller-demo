﻿using alfred.data.domain.entities;

namespace alfred.service.Common
{
    public interface IWalletService
    {
        Wallet GetById(int id);
        Wallet GetWalletForInstitution(Organization institution);
        void Create(Wallet wallet);
        void Update(Wallet wallet);
        void Delete(Wallet wallet);

        Wallet NewTransaction(Organization entity, decimal amount, string comment, Event usedWithEvent = null, decimal usedAmount = 0M);
    }
}