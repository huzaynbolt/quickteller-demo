﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;

namespace alfred.service.Common
{
    public class HandlerRestQueueService : IHandlerRestQueueService
    {
        private readonly IEventHandlerRestQueueRepository _restQueueRepository;
        private readonly IUnitOfWork _unitOfWork;
        public HandlerRestQueueService(IEventHandlerRestQueueRepository eventHandlerRepository ,
            IUnitOfWork unitOfWork)
        {
            _restQueueRepository = eventHandlerRepository;
            _unitOfWork = unitOfWork;
        }


        public void Insert(HandlerRestQueue handlerRestQueue)
        {
            _restQueueRepository.Add(handlerRestQueue);
            Save();
        }

        public void Update(HandlerRestQueue handlerRestQueue)
        {
            _restQueueRepository.Update(handlerRestQueue);
            Save();
        }

        public IPagedList<HandlerRestQueue> Search(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, List<int> psIds = null, int pageIndex = 0,
            int pageSize = Int32.MaxValue)
        {
            var query = _restQueueRepository.GetAll();

          

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOn);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOn);

            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.StatusId));



           
            query = query.OrderByDescending(o => o.CreatedOn);

            //database layer paging
            return new PagedResult<HandlerRestQueue>(query.AsQueryable(), pageIndex, pageSize);
        }

        private void Save()
        {
            _unitOfWork.Commit();
        }
    }

    public interface IHandlerRestQueueService {
        void Insert(HandlerRestQueue handlerRestQueue);
        void Update(HandlerRestQueue handlerRestQueue);

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Orders</returns>
        IPagedList<HandlerRestQueue> Search(
           
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> psIds = null,

            int pageIndex = 0, int pageSize = int.MaxValue);
    }
}