using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Caching;
using alfred.service.Helpers;

namespace alfred.service.Common
{
    public class UserInformationService : IUserInformationService
    {
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        // ReSharper disable once InconsistentNaming
        private const string USERROLES_ALL_KEY = "alfred.userrole.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : system name
        /// </remarks>
        // ReSharper disable once InconsistentNaming
        private const string USERROLES_BY_SYSTEMNAME_KEY = "alfred.userrole.systemname-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private const string USERROLES_PATTERN_KEY = "alfred.userrole.";


        private readonly IUserInformationRepository _userRepository;
        private readonly IUserRoleRepository _roleRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICacheManager _cacheManager;
        private readonly IDateTimeHelper _dateTimeHelper;
        public UserInformationService(IUnitOfWork unitOfWork,
            IUserInformationRepository userRepository,
            IUserRoleRepository roleRepository, 
            ICacheManager cacheManager,
            IDateTimeHelper dateTimeHelper)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _cacheManager = cacheManager;
            _dateTimeHelper = dateTimeHelper;
        }

        public UserInformation GetByUserId(string id)
        {
            return _userRepository.Get(x => x.UserId == id);
        }

        public UserInformation GetById(int id)
        {
            return _userRepository.GetById(id);
        }

        public void Create(UserInformation user)
        {
            if (user == null) throw new ArgumentNullException("user");
            user.CreatedOn = _dateTimeHelper.ConvertToUtcTime(DateTime.Now);
            _userRepository.Add(user);
            Save();
        }



        public List<UserInformation> GetAll()
        {
            return _userRepository.GetAll().ToList();
        }

        public IList<UserRole> GetUserRoles(bool showHidden = false)
        {
            return _roleRepository.GetAll().Where(x => (showHidden || x.Active)).ToList();
        }

        public UserRole GetUserRoleById(long userRoleId)
        {
            return userRoleId == 0 ? null : _roleRepository.GetById(userRoleId);
        }

        public UserRole GetUserRoleBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var key = string.Format(USERROLES_BY_SYSTEMNAME_KEY, systemName);
            return _cacheManager.Get(key, () =>
            {
                var query = _roleRepository.GetAll().OrderBy(cr => cr.Id).Where(cr => cr.SystemName == systemName);
                var userRole = query.FirstOrDefault();
                return userRole;
            });
        }

        public UserRole GetUserRoleBySystemName(SystemRole role)
        {
            return GetUserRoleBySystemName(role.ToString());
        }

        public void Update(UserInformation user)
        {
            if (user == null) throw new ArgumentNullException("user");
            _userRepository.Update(user);
            Save();
        }

        public void DeleteUser(UserInformation user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.Deleted = true;
            Update(user);
        }

        /// <summary>
        /// Get users by identifiers
        /// </summary>
        /// <param name="usersIds">User identifiers</param>
        /// <returns>Users</returns>
        public virtual IList<UserInformation> GetUsersByIds(long[] usersIds)
        {
            if (usersIds == null || usersIds.Length == 0)
                return new List<UserInformation>();

            var query = _userRepository.GetAll().Where(c => usersIds.Contains(c.Id));
            var users = query.ToList();
            //sort by passed identifiers
            return usersIds.Select(id => users.Find(x => x.Id == id)).Where(user => user != null).ToList();
        }

        public IPagedList<UserInformation> GetAllUsers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, long instituitionId = 0, long[] userRoleIds = null, string email = null, string username = null, string firstName = null, string lastName = null, string phone = null, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _userRepository.GetAll();
            if (createdFromUtc.HasValue)
                query = query.Where(c => createdFromUtc.Value <= c.CreatedOn);
            if (createdToUtc.HasValue)
                query = query.Where(c => createdToUtc.Value >= c.CreatedOn);
            if (instituitionId > 0)
                query = query.Where(c => instituitionId == c.OrganizationId);
            query = query.Where(c => !c.Deleted);
            if (userRoleIds != null && userRoleIds.Length > 0)
                query = query.Where(c => userRoleIds.Contains(c.UserRoleId));
            if (!string.IsNullOrWhiteSpace(email))
                query = query.Where(c => c.Email.Contains(email));
            if (!string.IsNullOrWhiteSpace(username))
                query = query.Where(c => c.User.UserName.Contains(username));

            if (!string.IsNullOrWhiteSpace(firstName))
            {
                query = query.Where(x => x.FirstName.Contains(firstName));
            }
            if (!string.IsNullOrWhiteSpace(lastName))
            {
                query = query.Where(x => x.LastName.Contains(lastName));
            }

            //search by phone
            if (!string.IsNullOrWhiteSpace(phone))
            {
                query = query.Where(x => x.PhoneNumber.Contains(phone));

            }
            query = query.OrderByDescending(c => c.CreatedOn);
            return new PagedResult<UserInformation>(query.AsQueryable(), pageIndex, pageSize);
        }

        public void DeleteUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            if (userRole.IsSystemRole)
                throw new Exception("System role could not be deleted");

            _roleRepository.Delete(userRole);
            Save();
            _cacheManager.RemoveByPattern(USERROLES_PATTERN_KEY);
        }

        public IList<UserRole> GetAllUserRoles(bool showHidden)
        {
            string key = string.Format(USERROLES_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = _roleRepository.GetAll().OrderBy(cr => cr.Name).Where(cr => (showHidden || cr.Active));
                var userRoles = query.ToList();
                return userRoles;
            });
        }

        public void InsertUserRole(UserRole userRole)
        {

            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _roleRepository.Add(userRole);

            _cacheManager.RemoveByPattern(USERROLES_PATTERN_KEY);
            Save();
        }

        public void UpdateUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _roleRepository.Update(userRole);
            Save();
            _cacheManager.RemoveByPattern(USERROLES_PATTERN_KEY);
        }

        public UserInformation GetUserBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var query = _userRepository.GetAll().OrderBy(c => c.Id).Where(c => c.Email == systemName);
            var user = query.FirstOrDefault();
            return user;
        }

        public IList<UserInformation> GetUsersBySystemName(string systemName)
        {
            var key = string.Format("GetUsersBySystemName_{0}",systemName);
            return _cacheManager.Get(key, () =>
            {
                var query = _userRepository.GetAll().Where(x=> x.UserRole.SystemName == systemName && !x.Deleted && x.Active);
                var users = query.ToList();
                return users;
            });
        }

        private void Save()
        {
            _unitOfWork.Commit();

        }
    }
}