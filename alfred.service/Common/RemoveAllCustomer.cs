﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.Extensions;
using alfred.service.Customers;
using alfred.service.Logging;
using alfred.service.ScheduleTasks;

namespace alfred.service.Common
{
    public class RemoveAllCustomer:IJob
    {
        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeService _attributeService;
        private readonly ILogger _logger;
        public RemoveAllCustomer(ICustomerAttributeService attributeService, ICustomerService customerService, ILogger logger)
        {
            _attributeService = attributeService;
            _customerService = customerService;
            _logger = logger;
        }

        public void Execute(IDictionary<string, string> taskParameters = null)
        {
           var result =    _customerService.GetAll();

           var customers =  result.ToList();
            foreach (var transaction in customers)
            {
                var attributes = _attributeService.GetAttributesForEntity(transaction.Id,
                    transaction.GetUnproxiedEntityType().Name);
                _attributeService.DeleteAttribute(attributes);
            }
            try
            {
                _customerService.Delete(customers);
            }
            catch (Exception e)
            {
                _logger.Error(e.Message,e);
            }
           

        }
    }
}