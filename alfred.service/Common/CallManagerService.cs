﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;

namespace alfred.service.Common
{
    public class CallManagerService: ICallManagerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICallLogRepository _callLogRepository;
        private readonly IRecurringCallRepository 
            _recurringCallRepository;
        public CallManagerService(IUnitOfWork unitOfWork, ICallLogRepository callLogRepository, IRecurringCallRepository recurringCallRepository)
        {
            _unitOfWork = unitOfWork;
            _callLogRepository = callLogRepository;
            _recurringCallRepository = recurringCallRepository;
        }

        public CallLog GetById(long id)
        {
            return _callLogRepository.GetById(id);
        }


        public CallLog GetById(string id)
        {
            return _callLogRepository.Get(x=>x.ApiIdentity == id);
        }

        public void Create(CallLog callLog)
        {
            _callLogRepository.Add(callLog);
            Save();
        }

        public void Update(CallLog callLog)
        {
            _callLogRepository.Update(callLog);
            Save();
        }

        public void Delete(CallLog callLog)
        {
            _callLogRepository.Delete(callLog);
            Save();
        }

        public IPagedList<CallLog> Search(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string appId = null,
            string customerId = null, string phone = null, List<int> psIds = null, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _callLogRepository.GetAll();
            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOn);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOn);

            if (!string.IsNullOrEmpty(appId))
            {
                query = query.Where(x => x.ApiIdentity == appId);
            }
            if (!string.IsNullOrEmpty(customerId))
            {
              query =  query.Where(x => x.Customer.CustomerId == customerId);
            }
            if (!string.IsNullOrEmpty(phone))
            {
                query = query.Where(x => x.MsIsdn == phone);
            }
            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.CallStatusId));


            
            query = query.OrderByDescending(o => o.CreatedOn);

            //database layer paging
            return new PagedResult<CallLog>(query.AsQueryable(), pageIndex, pageSize);
        }

        public void InsertRecurringCall(RecurringCall rp)
        {
           _recurringCallRepository.Add(rp);
            Save();
        }

        public void UpdateRecurringCall(RecurringCall rp)
        {
            _recurringCallRepository.Update(rp);
            Save();
        }

        public IList<RecurringCall> GetPendingRecurringCalls()
        {
            var now = DateTime.Now;

            var query = from t in _recurringCallRepository.GetAll()
                where t.NextRunDate.HasValue && t.NextRunDate <= now && t.IsActive
                orderby t.NextRunDate
                select t;

            return query.ToList();
        }

        public RecurringCall GetPendingRecurringCallById(long id)
        {
            return id <= 0 ? null : _recurringCallRepository.GetById(id);
        }

        public IPagedList<RecurringCall> SearchRecurringCalls(long institutionId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int pageIndex = 0, int pageSize = Int32.MaxValue, bool showHidden = false)
        {
            var query = _recurringCallRepository.GetAll();

            if (institutionId > 0)
                query = query.Where(o => o.InitialCallLog.Customer.OrganizationId == institutionId);




            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            query = query.Where(rp =>
                (showHidden || rp.IsActive));
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedResult<RecurringCall>(query.AsQueryable(), pageIndex, pageSize);
        }

        private void Save()
        {
            _unitOfWork.Commit();
        }
    }


    public interface ICallManagerService
    {
        CallLog GetById(long id);
        CallLog GetById(string id);
        void Create(CallLog callLog);
        void Update(CallLog callLog);
        void Delete(CallLog callLog);

        IPagedList<CallLog> Search
            (DateTime? createdFromUtc = null, DateTime? createdToUtc = null, string appId = null, string customerId = null, string phone = null, List<int> psIds = null, int pageIndex = 0, int pageSize = 2147483647);


        void InsertRecurringCall(RecurringCall rp);
        void UpdateRecurringCall(RecurringCall rp);

        IList<RecurringCall> GetPendingRecurringCalls();
        RecurringCall GetPendingRecurringCallById(long eventRecurringCallId);

        IPagedList<RecurringCall> SearchRecurringCalls(
            long institutionId = 0,

            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);
    }
}