﻿using System;
using System.Collections.Generic;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;

namespace alfred.service.Common
{
    public interface IUserInformationService
    {
        UserInformation GetByUserId(string id);

        UserInformation GetById(int id);
        void Create(UserInformation user);
        List<UserInformation> GetAll();

        UserRole GetUserRoleBySystemName(SystemRole role);

        void Update(UserInformation user);

        void DeleteUser(UserInformation user);

        IList<UserInformation> GetUsersByIds(long[] usersIds);

        /// <summary>
        ///  Gets all Users
        ///  </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="instituitionId">Vendor identifier</param>
        /// <param name="userRoleIds"></param>
        /// <param name="email">Email; null to load all customers</param>
        /// <param name="username">Username; null to load all customers</param>
        /// <param name="firstName">First name; null to load all customers</param>
        /// <param name="lastName">Last name; null to load all customers</param>
        /// <param name="phone">Phone; null to load all customers</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Customer collection</returns>
        IPagedList<UserInformation> GetAllUsers(DateTime? createdFromUtc = null, DateTime? createdToUtc = null, long instituitionId = 0, long[] userRoleIds = null, string email = null, string username = null, string firstName = null, string lastName = null, string phone = null, int pageIndex = 0, int pageSize = 2147483647);


        #region User roles

        /// <summary>
        /// Delete a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void DeleteUserRole(UserRole userRole);

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="userRoleId">User role identifier</param>
        /// <returns>User role</returns>
        UserRole GetUserRoleById(long userRoleId);

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="systemName">User role system name</param>
        /// <returns>User role</returns>
        UserRole GetUserRoleBySystemName(string systemName);

        /// <summary>
        /// Gets all user roles
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>User role collection</returns>
        IList<UserRole> GetAllUserRoles(bool showHidden = false);

        /// <summary>
        /// Inserts a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void InsertUserRole(UserRole userRole);

        /// <summary>
        /// Updates the user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void UpdateUserRole(UserRole userRole);

        #endregion

        UserInformation GetUserBySystemName(string systemName);

        IList<UserInformation> GetUsersBySystemName(string systemName);
    }
}