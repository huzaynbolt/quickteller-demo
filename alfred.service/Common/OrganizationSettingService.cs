﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.Common
{
    public class OrganizationSettingService : RepositoryService<OrganizationSetting>, IRepositoryServiceGet<OrganizationSetting>
    {
        public OrganizationSettingService(IOrganizationSettingRepository eventHandlerRepository , IUnitOfWork unitOfWork):base(eventHandlerRepository, unitOfWork)
        {
            
        }

        public IEnumerable<OrganizationSetting> Get(Expression<System.Func<OrganizationSetting, bool>> filter = null,
            System.Func<IQueryable<OrganizationSetting>, IOrderedQueryable<OrganizationSetting>> orderBy = null,
            string includeProperties = "")
        {
            var eventHandlers = GenericRepository.Get(filter, orderBy, includeProperties);
            return eventHandlers;
        }
    }
}