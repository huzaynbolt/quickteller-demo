﻿using System;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.Common
{
    public class WalletService: IWalletService
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepositoryService<Organization> _institutionService;

        public WalletService(IWalletRepository walletRepository, 
            IUnitOfWork unitOfWork, IRepositoryService<Organization> institutionService)
        {
            _walletRepository = walletRepository;
            _unitOfWork = unitOfWork;
            _institutionService = institutionService;
        }

        public Wallet GetById(int id)
        {
            if (id <= 0) throw new ArgumentOutOfRangeException("id");
            return _walletRepository.GetById(id);
        }

        public Wallet GetWalletForInstitution(Organization institution)
        {
            if (institution == null) throw new ArgumentNullException("institution");
            return _walletRepository.Get(x => x.OrganizationId == institution.Id);
        }

        public void Create(Wallet wallet)
        {
            if (wallet == null) throw new ArgumentNullException("wallet");
            _walletRepository.Add(wallet);
            Save();
        }

        public void Update(Wallet wallet)
        {
            if (wallet == null) throw new ArgumentNullException("wallet");
            _walletRepository.Update(wallet);
            Save();
        }

        public void Delete(Wallet wallet)
        {
            if (wallet == null) throw new ArgumentNullException("wallet");
            _walletRepository.Delete(wallet);
            Save();
        }

        public Wallet NewTransaction(Organization entity, decimal amount, string comment, Event usedWithEvent = null,
            decimal usedAmount = 0)
        {
            var wallet = GetWalletForInstitution(entity);
            if (wallet == null)
            {
                wallet = new Wallet()
                {
                    Amount = 0,
                    OrganizationId = entity.Id,
                };

                Create(wallet);
                entity.WalletId = wallet.Id;
                _institutionService.Update(entity);
            }
            var newAmount = wallet.Amount+ amount;
            var transactionType = newAmount > wallet.Amount? TransactionType.Credit :  TransactionType.Debit;
            wallet.Amount = newAmount;
            wallet.WalletHistories.Add(new WalletHistory()
            {
                Comment = comment,
                CreatedOn = DateTime.Now,
                TransactionType = transactionType,
                UsedAmount = usedAmount,
                UsedWithEvent = usedWithEvent,
                Balance = newAmount
            });

            Update(wallet);
            return wallet; 
        }

       
        private void Save()
        {
            _unitOfWork.Commit();
        }
    }
}