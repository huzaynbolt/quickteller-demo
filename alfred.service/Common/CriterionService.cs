﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.Common
{
    public class CriterionService:RepositoryService< Criterion>, IRepositoryServiceGet< Criterion>
    {
        public CriterionService(ICriterionRepository criterionRepository, IUnitOfWork unitOfWork):base(criterionRepository, unitOfWork)
        {
            
        }
        public IEnumerable<Criterion> Get(Expression<Func<Criterion, bool>> filter = null, Func<IQueryable<Criterion>, IOrderedQueryable<Criterion>> orderBy = null, string includeProperties = "")
        {
            var criteria = GenericRepository.Get(filter, orderBy, includeProperties);
            return criteria;
        }
    }
}