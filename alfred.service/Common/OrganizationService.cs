﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.Common
{


    public class OrganizationService : RepositoryService<Organization>, IRepositoryServiceGet<Organization>
    {
            public OrganizationService(IOrganizationRepository instituitionRepository, IUnitOfWork unitOfWork)
            : base(instituitionRepository, unitOfWork)
        {

        }

        public IEnumerable<Organization> Get(Expression<Func<Organization, bool>> filter = null,
             Func<IQueryable<Organization>, IOrderedQueryable<Organization>> orderBy = null,
             string includeProperties = "")
        {
            var instituitions = GenericRepository.Get(filter, orderBy, includeProperties);
            return instituitions;
        }


    }





    public static class OrganizationExtensions
    {
        /// <summary>
        /// Parse comma-separated Hosts
        /// </summary>
        /// <param name="organization">organization</param>
        /// <returns>Comma-separated hosts</returns>
        public static string[] ParseHostValues(this Organization organization)
        {
            if (organization == null)
                throw new ArgumentNullException("organization");

            var parsedValues = new List<string>();
            if (!String.IsNullOrEmpty(organization.Hosts))
            {
                string[] hosts = organization.Hosts.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string host in hosts)
                {
                    var tmp = host.Trim();
                    if (!String.IsNullOrEmpty(tmp))
                        parsedValues.Add(tmp);
                }
            }
            return parsedValues.ToArray();
        }

        /// <summary>
        /// Indicates whether a organization contains a specified host
        /// </summary>
        /// <param name="organization">organization</param>
        /// <param name="host">Host</param>
        /// <returns>true - contains, false - no</returns>
        public static bool ContainsHostValue(this Organization organization, string host)
        {
            if (organization == null)
                throw new ArgumentNullException("organization");

            if (String.IsNullOrEmpty(host))
                return false;

            var contains = organization.ParseHostValues()
                               .FirstOrDefault(x => x.Equals(host, StringComparison.InvariantCultureIgnoreCase)) != null;
            return contains;
        }
    }


}