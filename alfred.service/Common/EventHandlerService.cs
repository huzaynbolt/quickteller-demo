﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.Common
{
    public class EventHandlerService:RepositoryService<EventHandler>, IRepositoryServiceGet<EventHandler>
    {
        public EventHandlerService(IEventHandlerRepository  eventHandlerRepository , IUnitOfWork unitOfWork):base(eventHandlerRepository, unitOfWork)
        {
            
        }

        public IEnumerable<EventHandler> Get(Expression<System.Func<EventHandler, bool>> filter = null,
            System.Func<IQueryable<EventHandler>, IOrderedQueryable<EventHandler>> orderBy = null,
            string includeProperties = "")
        {
            var eventHandlers = GenericRepository.Get(filter, orderBy, includeProperties);
            return eventHandlers;
        }
    }
}