﻿namespace alfred.service.Common
{
    public class SystemUserAttributeNames
    {
        public static string WorkingThemeName { get { return "WorkingThemeName"; } }
        public static string AdminAreaOrganizationScopeConfiguration { get { return "AdminAreaOrganizationScopeConfiguration"; } }
    }
}