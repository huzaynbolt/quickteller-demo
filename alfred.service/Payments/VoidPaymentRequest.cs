using alfred.data.domain.entities;

namespace alfred.service.Payments
{
    public class VoidPaymentRequest
    {
        /// <summary>
        /// Gets or sets a Settlement
        /// </summary>
        public CustomerTransaction Shipment { get; set; }
    }
}