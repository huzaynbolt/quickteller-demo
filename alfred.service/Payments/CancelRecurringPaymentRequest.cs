﻿using alfred.data.domain.entities;

namespace alfred.service.Payments
{
    public class CancelRecurringPaymentRequest
    {
        public CustomerTransaction Shipment { get; set; }
    }
}