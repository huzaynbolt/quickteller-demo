using System.Collections.Generic;
using alfred.service.Configurations;

namespace alfred.service.Payments
{
    public class PaymentSettings : ISettings
    {
        public PaymentSettings()
        {
            ActivePaymentMethodSystemNames = new List<string>();
        }


        /// <summary>
        /// Gets or sets a system names of active payment methods
        /// </summary>
        public List<string> ActivePaymentMethodSystemNames { get; set; }

        public bool BypassPaymentMethodSelectionIfOnlyOne { get; set; }
        public bool SkipPaymentInfoStepForRedirectionPaymentMethods { get; set; }
        public bool ShowPaymentMethodDescriptions { get; set; }
    }
}