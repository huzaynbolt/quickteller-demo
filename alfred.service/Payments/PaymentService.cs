﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.domain.enums;

namespace alfred.service.Payments
{
    public class PaymentService:IPaymentService
    {
        private readonly PaymentSettings _paymentSettings;
        private readonly IPaymentMethodProvider _paymentMethodProvider;

        public PaymentService(PaymentSettings paymentSettings,
            IPaymentMethodProvider paymentMethodProvider)
        {
            _paymentSettings = paymentSettings;
            _paymentMethodProvider = paymentMethodProvider;
        }

        public IList<IPaymentMethod> LoadActivePaymentMethods()
        {
            return  LoadAllPaymentMethods().Where(x=> _paymentSettings.ActivePaymentMethodSystemNames.Contains(x.SystemName)).ToList();
        }

        public IPaymentMethod LoadPaymentMethodBySystemName(string systemName)
        {
            return _paymentMethodProvider.GetPayment(systemName);
        }

        public IList<IPaymentMethod> LoadAllPaymentMethods()
        {
            return _paymentMethodProvider.GetPayments() ;
        }

        public PreProcessPaymentResult PreProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest.AmountTotal == decimal.Zero)
            {
                var result = new PreProcessPaymentResult();
                return result;
            }
            var paymentMethod = LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new Exception("Payment method couldn't be loaded");

            return paymentMethod.PreProcessPayment(processPaymentRequest);
        }

        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest.AmountTotal == decimal.Zero)
            {
                var result = new ProcessPaymentResult
                {
                    NewPaymentStatus = PaymentStatus.Authorized
                };
                return result;
            }


            var paymentMethod = LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new Exception("Payment method couldn't be loaded");
            return paymentMethod.ProcessPayment(processPaymentRequest);
        }

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            if (!string.IsNullOrEmpty(postProcessPaymentRequest.Shipment.PaymentMethodSystemName))
            {
                var paymentMethod = LoadPaymentMethodBySystemName(postProcessPaymentRequest.Shipment.PaymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method couldn't be loaded");

                paymentMethod.PostProcessPayment(postProcessPaymentRequest);
            }
        }

        public bool CanRePostProcessPayment(CustomerTransaction shipment)
        {
            return false;
        }

        public bool SupportCapture(string paymentMethodSystemName)
        {
            return false;
        }

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var paymentMethod = LoadPaymentMethodBySystemName(capturePaymentRequest.Shipment.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new Exception("Payment method couldn't be loaded");
            return paymentMethod.Capture(capturePaymentRequest);
        }

        public bool SupportPartiallyRefund(string paymentMethodSystemName)
        {
            return false;
        }

        public bool SupportRefund(string paymentMethodSystemName)
        {
            return false;
        }

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            throw new NotImplementedException();
        }

        public bool SupportVoid(string paymentMethodSystemName)
        {
            return false;
        }

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            throw new NotImplementedException();
        }

        public RecurringPaymentType GetRecurringPaymentType(string paymentMethodSystemName)
        {
            var paymentMethod = LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return RecurringPaymentType.NotSupported;
            return paymentMethod.RecurringPaymentType;
        }

        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest.AmountTotal == decimal.Zero)
            {
                var result = new ProcessPaymentResult
                {
                    NewPaymentStatus = PaymentStatus.Paid
                };
                return result;
            }

            var paymentMethod = LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new Exception("Payment method couldn't be loaded");
            return paymentMethod.ProcessRecurringPayment(processPaymentRequest);
        }

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            if (cancelPaymentRequest.Shipment.Amount == decimal.Zero)
                return new CancelRecurringPaymentResult();

            var paymentMethod = LoadPaymentMethodBySystemName(cancelPaymentRequest.Shipment.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new Exception("Payment method couldn't be loaded");
            return paymentMethod.CancelRecurringPayment(cancelPaymentRequest);
        }

        public PaymentMethodType GetPaymentMethodType(string paymentMethodSystemName)
        {
            throw new NotImplementedException();
        }

        public string GetMaskedCreditCardNumber(string creditCardNumber)
        {
            if (String.IsNullOrEmpty(creditCardNumber))
                return string.Empty;

            if (creditCardNumber.Length <= 4)
                return creditCardNumber;

            string last4 = creditCardNumber.Substring(creditCardNumber.Length - 4, 4);
            string maskedChars = string.Empty;
            for (int i = 0; i < creditCardNumber.Length - 4; i++)
            {
                maskedChars += "*";
            }
            return maskedChars + last4;
        }

        public decimal GetAdditionalHandlingFee(CustomerTransaction shipment, string paymentMethodSystemName)
        {
            if (String.IsNullOrEmpty(paymentMethodSystemName))
                return decimal.Zero;

            var paymentMethod = LoadPaymentMethodBySystemName(paymentMethodSystemName);
            if (paymentMethod == null)
                return decimal.Zero;

            decimal result = paymentMethod.GetAdditionalHandlingFee(shipment);
            if (result < decimal.Zero)
                result = decimal.Zero;
            
            return result;
        }
    }
}