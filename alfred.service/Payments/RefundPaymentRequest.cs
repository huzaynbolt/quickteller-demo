using alfred.data.domain.entities;

namespace alfred.service.Payments
{
    public class RefundPaymentRequest
    {
        /// <summary>
        /// Gets or sets an settlement
        /// </summary>
        public CustomerTransaction Shipment { get; set; }

        /// <summary>
        /// Gets or sets an amount
        /// </summary>
        public decimal AmountToRefund { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's a partial refund; otherwize, full refund
        /// </summary>
        public bool IsPartialRefund { get; set; }
    }
}