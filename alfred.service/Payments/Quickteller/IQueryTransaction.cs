﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alfred.service.Payments.Quickteller
{
    public interface IQueryTransaction
    {
        //QueryTransactionResponse Query(QuickTellerTransactionData data);

        object Query(QuickTellerTransactionData data);

        string GetTransactionReference(string transactionReference);
    }
}
