﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using alfred.service.Logging;
using alfred.service.RestClient;

namespace alfred.service.Payments.Quickteller
{ 
    public class InquireTransaction : IInquireTransaction
    {
        private readonly IServiceConsumer _serviceConsumer;
        private readonly RequestHeadersUtil _requestHeadersUtil;
        private readonly InterSwitchOauthCredentials _switchOauthCredentials;
        public InquireTransaction(IServiceConsumer serviceConsumer, RequestHeadersUtil requestHeadersUtil,InterSwitchOauthCredentials switchOauthCredentials)
        {
            this._serviceConsumer = serviceConsumer;
            this._requestHeadersUtil = requestHeadersUtil;
            this._switchOauthCredentials = switchOauthCredentials;
        }
        public InquireTransactionResponse Inquire(InquiryData data)
        {
            try
            {
                string url =
                    $"https://sandbox.interswitchng.com/api/v2/quickteller/transactions/{data.CustomerId}/inquiry";
                var headers = _requestHeadersUtil.GetHeaders("POST", url, clientId: _switchOauthCredentials.ClientId, secretKey: _switchOauthCredentials.SecretKey, accessToken: _switchOauthCredentials.AccessToken,
                    signedParam: "");
                var response = _serviceConsumer.Post<InquireTransactionResponse>(headers, data, url, true,true);
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
           
        }
    }
}
