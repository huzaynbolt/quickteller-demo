﻿using Newtonsoft.Json;

namespace alfred.service.Payments.Quickteller
{
    public class InquiryData
    {
        [JsonProperty("paymentCode")]
        public string PaymentCode { get; set; }

        [JsonProperty("customerId")]
        public string CustomerId { get; set; }

        [JsonProperty("pageFlowValues")]
        public string PageFlowValues { get; set; }

        [JsonProperty("CustomerEmail")]
        public string CustomerEmail { get; set; }

        [JsonProperty("CustomerMobile")]
        public string CustomerMobile { get; set; }

        [JsonProperty("TerminalId")]
        public string TerminalId { get; set; }
    }
}