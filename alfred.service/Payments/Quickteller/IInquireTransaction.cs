﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alfred.service.Payments.Quickteller
{
    public interface IInquireTransaction
    {
        

        InquireTransactionResponse Inquire(InquiryData data);

    }
}
