﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alfred.service.Payments.Quickteller
{
    public interface IQuickTellerPaymentTransaction
    {
         PaymentTransactionResponse SendTransaction(UserTransactionData transactionData);

      

        PaymentTransactionResponse SendTransactionWithOneTimeToken(UserTransactionData transactionData, string token);

         
    } 
    
}