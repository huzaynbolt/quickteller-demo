﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using alfred.service.Payments.Quickteller.InterswitchUtilities;
using Interswitch;

namespace alfred.service.Payments.Quickteller
{
    public static class PaymentTransactionsExtensions
    {
        public static QuickTellerTransactionData GetQuickTellerTransactionData(this UserTransactionData userTransaction)
        {
            var quickTellerTransactionData = new QuickTellerTransactionData();

            quickTellerTransactionData.Amount = userTransaction.Amount;
            quickTellerTransactionData.Msisdn = userTransaction.Msisdn;
            quickTellerTransactionData.CardBin = userTransaction.CardBin;
            quickTellerTransactionData.TransactionRef = userTransaction.TransactionRef;
            quickTellerTransactionData.SecureData = SecurityUtils.GetSecureData(userTransaction.PanNumber,userTransaction.TransactionRef,userTransaction.Amount, userTransaction.Msisdn,userTransaction.PinNumber,userTransaction.Cvv,userTransaction.Expiry);
            quickTellerTransactionData.PinData = SecurityUtils.getEncryptedPinCvv2ExpiryDateBlockWithoutPin(userTransaction.PinNumber,userTransaction.Cvv,userTransaction.Expiry);


            return quickTellerTransactionData;
        }
    }

    
}
