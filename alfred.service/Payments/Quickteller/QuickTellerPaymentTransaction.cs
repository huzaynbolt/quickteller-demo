﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using alfred.service.RestClient;

namespace alfred.service.Payments.Quickteller
{
    public class QuickTellerPaymentTransaction:IQuickTellerPaymentTransaction
    {
        private readonly IServiceConsumer _serviceConsumer;
        private readonly RequestHeadersUtil _requestHeadersUtil;
        private readonly InterSwitchOauthCredentials _credentials;
        public QuickTellerPaymentTransaction(IServiceConsumer serviceConsumer, RequestHeadersUtil requestHeadersUtil, InterSwitchOauthCredentials credentials)
        {
            this._serviceConsumer = serviceConsumer;
            this._requestHeadersUtil = requestHeadersUtil;
            this._credentials = credentials;
        }
        public PaymentTransactionResponse
            SendTransaction(UserTransactionData transactionData)
        {
            string url =
                $"https://sandbox.interswitchng.com/api/v2/quickteller/transactions";
            var headers = _requestHeadersUtil.GetHeaders("POST", url, clientId: _credentials.ClientId, secretKey: _credentials.SecretKey, accessToken: _credentials.AccessToken,
                signedParam: "");

            var quickTellerTransactionData = transactionData.GetQuickTellerTransactionData();

            var response = _serviceConsumer.Post<PaymentTransactionResponse>(headers, quickTellerTransactionData, url, true,true);
            return response;
        }

        public PaymentTransactionResponse SendTransactionWithOneTimeToken(UserTransactionData transactionData, string token)
        {
            string url =
                $"https://sandbox.interswitchng.com/api/v2/quickteller/transactions";
            var headers = _requestHeadersUtil.GetHeaders("POST", url, clientId: _credentials.ClientId, secretKey: _credentials.SecretKey, accessToken: _credentials.AccessToken,
                signedParam: "");

            var quickTellerTransactionData = transactionData.GetQuickTellerTransactionData();
             
            var response = _serviceConsumer.Post<PaymentTransactionResponse>(headers, quickTellerTransactionData, url, true,true);
            return response;
        }

        public PaymentTransactionResponse VerifyOTPTransactions(string transactionId,string otpToken)
        {
            string url =
                $"https://sandbox.interswitchng.com/api/v2/quickteller/transactions/authoriseotp";
            var headers = _requestHeadersUtil.GetHeaders("POST", url, clientId: _credentials.ClientId, secretKey: _credentials.SecretKey, accessToken: _credentials.AccessToken,
                signedParam: "");

            var newData = new { otp=otpToken, transactionIdentifier= transactionId };

            var response = _serviceConsumer.Post<PaymentTransactionResponse>(headers, newData, url, true,true);
            return response;
        }



    }
}
