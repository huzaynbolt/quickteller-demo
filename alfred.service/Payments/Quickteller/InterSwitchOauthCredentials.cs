﻿namespace alfred.service.Payments.Quickteller
{
    public class InterSwitchOauthCredentials
    {
        public string ClientId { get; set; }

        public string SecretKey { get; set; }

        public string AccessToken { get; set; }
    }
}