﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace alfred.service.Payments.Quickteller
{
    public class InquireTransactionResponse:ResponseErrors
    {
        public InquireTransactionResponse()
        {
            errors = new List<Error>();
        }

        public string TransactionRef { get; set; }
        public string Biller { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PaymentItem { get; set; }
        public string Narration { get; set; }
        public string Amount { get; set; }
        public string IsAmountFixed { get; set; }
        public string CollectionsAccountNumber { get; set; }
        public string CollectionsAccountType { get; set; }
        public string Surcharge { get; set; }
        public string ResponseCode { get; set; }
        public string ShortTransactionRef { get; set; }
    }


    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ResponseErrors
    {
        public List<Error> errors { get; set; }
        public Error error { get; set; }
    }

}
