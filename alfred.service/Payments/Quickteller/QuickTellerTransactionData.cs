﻿using Newtonsoft.Json;

namespace alfred.service.Payments.Quickteller
{
    public partial class QuickTellerTransactionData
    {
        [JsonProperty("transactionRef")]
        public string TransactionRef { get; set; }

        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("pinData")]
        public string PinData { get; set; }

        [JsonProperty("cardBin")]
        public string CardBin { get; set; }

        [JsonProperty("secureData")]
        public string SecureData { get; set; }
    }


    public class UserTransactionData
    {
        /// <summary>
        /// Gets or sets the transaction reference.
        /// </summary>
        /// <value>
        /// The transaction reference of the initiated transaction.
        /// </value>
        [JsonProperty("transactionRef")]
        public string TransactionRef { get; set; }

        /// <summary>
        /// Gets or sets the msisdn.
        /// </summary>
        /// <value>
        /// The user msisdn (phone number/ email).
        /// </value>
        [JsonProperty("msisdn")]
        public string Msisdn { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount of the current transaction.
        /// </value>
        [JsonProperty("amount")]
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets the pan number.
        /// </summary>
        /// <value>
        /// The number inscribed on the credit card .
        /// </value>
        [JsonProperty("pan_number")]
        public string PanNumber { get; set; }

        /// <summary>
        /// Gets or sets the CVV.
        /// </summary>
        /// <value>
        /// The CVV (3 0r 4 digit at the back of card).
        /// </value>
        [JsonProperty("cvv")]
        public string Cvv { get; set; }

        /// <summary>
        /// Gets or sets the pin number.
        /// </summary>
        /// <value>
        /// The pin number attached to the card known by card owner.
        /// </value>
        [JsonProperty("pin")]
        public string PinNumber { get; set; }

        /// <summary>
        /// Gets or sets the expiry.
        /// </summary>
        /// <value>
        /// The expiry data (02/05).
        /// </value>
        [JsonProperty("expiry")]
        public string Expiry { get; set; }


        /// <summary>
        /// Gets the card bin.
        /// </summary>
        /// <value>
        /// The card bin- The first 6 digits from <see cref="PanNumber"/>.
        /// </value>
        public string CardBin => string.IsNullOrEmpty(PanNumber) || PanNumber.Length<=6 ? "000000" : PanNumber.Substring(0, 6);


    }

}