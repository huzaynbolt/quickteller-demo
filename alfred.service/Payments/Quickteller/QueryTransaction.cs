﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;using alfred.service.Payments.Quickteller;
using alfred.service.RestClient;

namespace alfred.service.Payments.Quickteller
{
    public class QueryTransaction:IQueryTransaction
    {
        private readonly IServiceConsumer _serviceConsumer;
        private readonly RequestHeadersUtil _requestHeadersUtil;
        private readonly InterSwitchOauthCredentials _credentials;

        public QueryTransaction(IServiceConsumer serviceConsumer, RequestHeadersUtil requestHeadersUtil, InterSwitchOauthCredentials credentials)
        {
            this._serviceConsumer = serviceConsumer;
            this._requestHeadersUtil = requestHeadersUtil;
            this._credentials = credentials;
        }

        public string GetTransactionReference(string transactionReference)
        {
           
                if (!transactionReference.Contains("|"))
                {
                    return transactionReference;
                }
                var arrayRef = transactionReference.Split('|');


                return arrayRef.Length >=5 ? arrayRef[5] : transactionReference;
            
        }

        public object Query(QuickTellerTransactionData data)
        {
            string reference = GetTransactionReference(data.TransactionRef);
            string url =
                $"https://sandbox.interswitchng.com/api/v2/quickteller/transactions?requestreference={reference}";
            var headers = _requestHeadersUtil.GetHeaders("GET", url, clientId: _credentials.ClientId, secretKey: _credentials.SecretKey, accessToken: _credentials.AccessToken,
                signedParam: "");


            var response = _serviceConsumer.Get<object>(headers, null, url, true);
            return response;
        }
    }
}
