﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interswitch;

namespace alfred.service.Payments.Quickteller
{
    /// <summary>
    /// This generates all necessary request Headers as required by Quickteller specification
    /// </summary>
    public class RequestHeadersUtil
    {
        public RequestHeadersUtil(string contentType, string terminalId)
        {
            ContentType = contentType;
            TerminalId = terminalId;
        }
        public string ContentType { get; set; }

        public string TerminalId { get; set; }

        public NameValueCollection GetHeaders(string httpVerb, string url, string clientId, string secretKey,string accessToken, string signedParam)
        {
            Config config = new Config(
                httpVerb,url,clientId,secretKey,accessToken,signedParam);

            var headers = new NameValueCollection(this.GetType().GetProperties().Length);

            try
            {
                headers.Add("ContentType",ContentType);

               // headers.Add("Authorization",config.GetAuthorization());

                byte[] bytes = Encoding.UTF8.GetBytes(clientId);
                string base64 = Convert.ToBase64String(bytes);
      
                headers.Add("Authorization", "InterswitchAuth " + base64);

                headers.Add("Signature",config.GetSignature());

                headers.Add("Nonce",config.GetNonce());

              

                headers.Add("TimeStamp",config.GetTimeStamp().ToString());

               

                headers.Add("SignatureMethod", "SHA1");

                headers.Add("TerminalId",TerminalId);

                return headers;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message + " Likely dupicate of headers present in collection ");
            }

        }

        
    }
}
