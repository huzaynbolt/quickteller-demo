using System.Collections.Generic;

namespace alfred.service.Payments
{
    public interface IPaymentMethodProvider
    {
        IList<IPaymentMethod> GetPayments();
        IPaymentMethod GetPayment(string systemName);
    }
}