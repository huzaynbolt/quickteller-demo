using alfred.data.domain.entities;

namespace alfred.service.Payments
{
    public class CapturePaymentRequest
    {
        public CustomerTransaction Shipment { get; set; }
    }
}