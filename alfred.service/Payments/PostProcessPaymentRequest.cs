using alfred.data.domain.entities;

namespace alfred.service.Payments
{
    public class PostProcessPaymentRequest
    {
        public CustomerTransaction Shipment { get; set; }
        public string ClientToken { get; set; }

        public string AuthorizationTransactionId { get; set; }
        public string RedirectUrl { get; set; }
    }
}