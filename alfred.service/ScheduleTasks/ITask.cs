﻿using System.Collections.Generic;

namespace alfred.service.ScheduleTasks
{
    /// <summary>
    /// Interface that should be implemented by each task
    /// </summary>
    public interface IJob
    {
        /// <summary>
        /// Execute task
        /// </summary>
        void Execute(IDictionary<string, string> taskParameters = null);
    }
}
