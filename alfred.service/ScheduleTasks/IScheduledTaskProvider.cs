﻿namespace alfred.service.ScheduleTasks
{
    public interface IScheduledTaskProvider
    {
        IJob GetTask(string key);
    }
}