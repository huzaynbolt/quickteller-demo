﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.service.Common;
using alfred.service.Infrastructure;
using alfred.service.Logging;
using alfred.service.Security;

namespace alfred.service.ScheduleTasks
{
    public class TaskExecutor:ITaskExecutor
    {
        private readonly IScheduleTaskService _scheduledTaskService;
        private readonly IUserInformationService _userInformationService;
        public const string CurrentUserIdParamName = "CurrentUserId";
        private readonly IWorkContext _workContext;
        private readonly IScheduledTaskProvider _taskProvider;
        private readonly ILogger _logger;

        public TaskExecutor(IScheduleTaskService scheduledTaskService,
            IUserInformationService userInformationService,
            IWorkContext workContext, IScheduledTaskProvider taskProvider,
            ILogger logger)
        {
            _scheduledTaskService = scheduledTaskService;
            _userInformationService = userInformationService;
            _workContext = workContext;
            _taskProvider = taskProvider;
            _logger = logger;
        }

        public void Execute(
            ScheduleTask task,
            IDictionary<string, string> taskParameters = null,
            bool throwOnError = false)
        {
            if (task.IsRunning)
                return;

            

            bool faulted = false;
            bool canceled = false;
            string lastError = null;
            IJob instance = null;

            try
            {

                var taskKey = _taskProvider.GetTask(task.Key);

                Debug.WriteLineIf(taskKey == null, "Invalid task key: " + task.Key);

                if (taskKey == null)
                    return;

            }
            catch (System.Exception e)
            {
                _logger.Error(e.Message,e);
                return;
            }

            DateTime start = DateTime.Now;
            try
            {
                UserInformation user = null;

                // try virtualize current customer (which is necessary when user manually executes a task)
                if (taskParameters != null && taskParameters.ContainsKey(CurrentUserIdParamName))
                {
                    user = _userInformationService.GetByUserId(taskParameters[CurrentUserIdParamName]);
                }

                if (user == null)
                {
                    // no virtualization: set background task system user as current user
                    user = _userInformationService.GetUserBySystemName(SystemUserRoleNames.BackgroundTask);
                }

                _workContext.CurrentUser = user;

                // create task instance
                instance = _taskProvider.GetTask(task.Key);

                // prepare and save entity
                task.LastStartUtc = start = DateTime.UtcNow;
                task.LastEndUtc = null;
                task.NextRunUtc = null;
                task.ProgressPercent = null;
                task.ProgressMessage = null;
                
                _scheduledTaskService.UpdateTask(task);



                var param = taskParameters ?? new Dictionary<string, string>();

                instance.Execute(param);
            }
            catch (Exception exception)
            {
                faulted = true;
                canceled = exception is OperationCanceledException;
                lastError = exception.Message;

                if (canceled)
                    _logger.Warning(string.Format("The scheduled task {0} has been canceled", task.Name), exception);
                else
                    _logger.Error(string.Concat(string.Format("Error while running scheduled task {0}", task.Name), ": ", exception.Message), exception);

                if (throwOnError)
                {
                    throw;
                }
            }
            finally
            {
               

                task.ProgressPercent = null;
                task.ProgressMessage = null;

                var now = DateTime.UtcNow;
                task.LastError = lastError;
                task.LastEndUtc = now;

                if (faulted)
                {
                    if ((!canceled && task.StopOnError) || instance == null)
                    {
                        task.Enabled = false;
                    }
                }
                else
                {
                    task.LastSuccessUtc = now;
                }

                if (task.Enabled)
                {
                    task.NextRunUtc = _scheduledTaskService.GetNextSchedule(task);
                }

                task.ScheduleTaskLogs.Add(new ScheduleTaskLog()
                {
                    EndDateTime = task.LastEndUtc.Value,
                    ScheduleTaskStatus = !faulted ? ScheduleTaskStatus.Success: ScheduleTaskStatus.Fail,
                    StartDateTime = start
                });
                 
                _scheduledTaskService.UpdateTask(task);
            }
        }
    }
}