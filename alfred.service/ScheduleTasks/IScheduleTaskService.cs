﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using alfred.data.domain.entities;

namespace alfred.service.ScheduleTasks
{
    public interface IScheduleTaskService
    {
        /// <summary>
        /// Deletes a task
        /// </summary>
        /// <param name="task">Task</param>
        void DeleteTask(ScheduleTask task);

        /// <summary>
        /// Gets a task
        /// </summary>
        /// <param name="taskId">Task identifier</param>
        /// <returns>Task</returns>
        ScheduleTask GetTaskById(long taskId);


        /// <summary>
        /// Gets all tasks
        /// </summary>
        /// <param name="includeDisabled">A value indicating whether to load disabled tasks also</param>
        /// <returns>Tasks</returns>
        IList<ScheduleTask> GetAllTasks(bool includeDisabled = false);

        /// <summary>
        /// Gets all pending tasks
        /// </summary>
        /// <returns>Tasks</returns>
        IList<ScheduleTask> GetPendingTasks();

        /// <summary>
        /// Gets a value indicating whether at least one task is running currently.
        /// </summary>
        /// <returns></returns>
        bool HasRunningTasks();

        /// <summary>
        /// Gets a value indicating whether a task is currently running
        /// </summary>
        /// <param name="taskId">A <see cref="ScheduleTask"/> identifier</param>
        /// <returns><c>true</c> if the task is running, <c>false</c> otherwise</returns>
        bool IsTaskRunning(int taskId);

        /// <summary>
        /// Gets a list of currently running <see cref="ScheduleTask"/> instances.
        /// </summary>
        /// <returns>Tasks</returns>
        IList<ScheduleTask> GetRunningTasks();

        /// <summary>
        /// Inserts a task
        /// </summary>
        /// <param name="task">Task</param>
        void InsertTask(ScheduleTask task);

        /// <summary>
        /// Updates the task
        /// </summary>
        /// <param name="task">Task</param>
        void UpdateTask(ScheduleTask task);

        

        /// <summary>
        /// Calculates - according to their cron expressions - all task future schedules
        /// and saves them to the database.
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="isAppStart">When <c>true</c>, determines stale tasks and fixes their states to idle.</param>
        void CalculateFutureSchedules(IEnumerable<ScheduleTask> tasks, bool isAppStart = false);

        /// <summary>
        /// Calculates the next schedule according to the task's cron expression
        /// </summary>
        /// <param name="task">ScheduleTask</param>
        /// <returns>The next schedule or <c>null</c> if the task is disabled</returns>
        DateTime? GetNextSchedule(ScheduleTask task);

        /// <summary>
        /// Get's Scheduled Task Logs By TaskId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IList<ScheduleTaskLog> GetScheduleTaskLogsByTaskId(int id);
        /// <summary>
        /// Get Schedule Task Logs By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ScheduleTaskLog GetScheduleTaskLogById(int id);


        Task<bool> RunTaskForKey(string taskKey);

        ScheduleTask GetTaskByKey(string key);
    }
}