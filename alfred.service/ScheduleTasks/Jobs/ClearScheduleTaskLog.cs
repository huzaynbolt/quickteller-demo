﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.service.Helpers;
using alfred.service.Infrastructure;

namespace alfred.service.ScheduleTasks.Jobs
{
    public class ClearScheduleTaskLog : IJob
    {
        private readonly IRepositoryService<ScheduleTaskLog> _repositoryService;


        public ClearScheduleTaskLog(IRepositoryService<ScheduleTaskLog> repositoryService)
        {
            _repositoryService = repositoryService;
        }

        public void Execute(IDictionary<string, string> taskParameters = null)
        {
            int olderThanDays = CommonHelper.GetAppSetting<int>("logOlderThan");

            if ( olderThanDays <= 0)
            {
                olderThanDays = 7;
            }
            var toUtc = DateTime.UtcNow.AddDays(-olderThanDays);

            var logs = _repositoryService.GetMany(x => x.StartDateTime <= toUtc).ToList();

            _repositoryService.Delete(logs);

        }
    }
}