﻿using System.Collections.Generic;
using alfred.service.Caching;
using alfred.service.eventbus.Abstractions;
using alfred.service.eventbus.Events;

namespace alfred.service.ScheduleTasks.Jobs
{
   public class ClearCache:IJob
   {
       private readonly IEventBus _eventBus;

       public ClearCache(IEventBus eventBus)
       {
           _eventBus = eventBus;
       }

       public  void Execute(IDictionary<string, string> taskParameters)
        {

            var cacheManager = new MemoryCacheManager();
            cacheManager.Clear();
            var theEvent = new ClearCacheEvent();
            _eventBus.Publish(theEvent);
        }
       
    }
}
