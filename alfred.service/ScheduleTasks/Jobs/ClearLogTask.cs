﻿using System;
using System.Collections.Generic;
using alfred.service.Helpers;
using alfred.service.Logging;

namespace alfred.service.ScheduleTasks.Jobs
{
    public class ClearLogTask : IJob
    {
        private readonly ILogger _logger;

        public ClearLogTask(ILogger logger)
           
        {
            _logger = logger;
        }

        /// <inheritdoc />
        /// <summary>
        /// Executes a task
        /// </summary>
        public  void Execute(IDictionary<string, string> taskParameters)
        {
            int olderThanDays = CommonHelper.GetAppSetting<int>("logOlderThan");

            if (olderThanDays <= 0)
            {
                olderThanDays = 7;
            }
            var toUtc = DateTime.UtcNow.AddDays(-olderThanDays);

            _logger.ClearLog(toUtc);
        }
    }
}