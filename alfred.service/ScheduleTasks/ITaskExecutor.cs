﻿using System.Collections.Generic;
using alfred.data.domain.entities;

namespace alfred.service.ScheduleTasks
{
    public interface ITaskExecutor
    {
        void Execute(
          ScheduleTask task,
          IDictionary<string, string> taskParameters = null,
          bool throwOnError = false);
    }
}