﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;
using alfred.data.Repository;

namespace alfred.service.ScheduleTasks
{
    public class ScheduleTaskService:IScheduleTaskService
    {
        #region Fields

        private readonly IScheduleTaskRepository _taskRepository;

        private readonly IUnitOfWork _unitOfWork;

        private readonly IScheduleTaskLogRepository _scheduledTaskLogRepository;
        private readonly IScheduledTaskProvider _provider;

        #endregion

        #region Ctor

        public ScheduleTaskService(IScheduleTaskRepository taskRepository, 
            IUnitOfWork unitOfWork,
            IScheduleTaskLogRepository scheduledTaskLogRepository,
            IScheduledTaskProvider provider)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
            _scheduledTaskLogRepository = scheduledTaskLogRepository;
            _provider = provider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a task
        /// </summary>
        /// <param name="task">Task</param>
        public virtual void DeleteTask(ScheduleTask task)
        {
            if (task == null)
                throw new ArgumentNullException("task");

            _taskRepository.Delete(task);
        }

        /// <summary>
        /// Gets a task
        /// </summary>
        /// <param name="taskId">Task identifier</param>
        /// <returns>Task</returns>
        public virtual ScheduleTask GetTaskById(long taskId)
        {
            if (taskId == 0)
                return null;

            return _taskRepository.GetById(taskId);
        }

        /// <summary>
        /// Gets a task by its key
        /// </summary>
        /// <param name="key">Task type</param>
        /// <returns>Task</returns>
        public virtual ScheduleTask GetTaskByKey(string key)
        {
            if (String.IsNullOrWhiteSpace(key))
                return null;

            var query = _taskRepository.GetAll();
            query = query.Where(st => st.Key == key);
            query = query.OrderByDescending(t => t.Id);

            var task = query.FirstOrDefault();
            return task;
        }

        /// <summary>
        /// Gets all tasks
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Tasks</returns>
        public virtual IList<ScheduleTask> GetAllTasks(bool showHidden = false)
        {
            var query = _taskRepository.GetAll();
            if (!showHidden)
            {
                query = query.Where(t => t.Enabled);
            }
            query = query.OrderByDescending(t => t.Enabled);

            var tasks = query.ToList();
            return tasks;
        }

        /// <summary>
        /// Inserts a task
        /// </summary>
        /// <param name="task">Task</param>
        public virtual void InsertTask(ScheduleTask task)
        {
            if (task == null)
                throw new ArgumentNullException("task");

            _taskRepository.Add(task);
            Save();
        }

        /// <summary>
        /// Updates the task
        /// </summary>
        /// <param name="task">Task</param>
        public virtual void UpdateTask(ScheduleTask task)
        {
            if (task == null)
                throw new ArgumentNullException("task");

            _taskRepository.Update(task);
            Save();
        }

        
        #endregion

        private void Save()
        {
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IList<ScheduleTaskLog> GetScheduleTaskLogsByTaskId(int id)
        {
            if (id <= 0) throw new ArgumentOutOfRangeException("id");

            return _scheduledTaskLogRepository.GetAll().Where(log=>log.ScheduleTaskId == id).ToList();
        }

        /// <summary>
        /// Get Schedule Task Log By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ScheduleTaskLog GetScheduleTaskLogById(int id)
        {
           return _scheduledTaskLogRepository.GetById(id);
        }

        public async Task<bool> RunTaskForKey(string taskKey)
        {
            var scheduledTask = GetTaskByKey(taskKey);
            if (scheduledTask == null)
                return await new Task<bool>(() => false);
            var task = _provider.GetTask(scheduledTask.Key);
            if (task != null)
            {
                var result = false;
                scheduledTask.LastStartUtc = DateTime.Now;
                try
                {

                    task.Execute();
                    scheduledTask.LastEndUtc = scheduledTask.LastSuccessUtc = DateTime.UtcNow;
                    scheduledTask.ScheduleTaskLogs.Add(new ScheduleTaskLog()
                    {
                        EndDateTime = scheduledTask.LastEndUtc.Value,
                        ScheduleTaskStatus = ScheduleTaskStatus.Success,
                        StartDateTime = scheduledTask.LastStartUtc.Value
                    }); 
                    result = true;
                }
                catch (System.Exception ex )
                {
                    scheduledTask.LastEndUtc  = DateTime.UtcNow;
                    scheduledTask.LastError = ex.Message;
                    scheduledTask.ScheduleTaskLogs.Add(new ScheduleTaskLog()
                    {
                        EndDateTime = scheduledTask.LastEndUtc.Value,
                        ScheduleTaskStatus = ScheduleTaskStatus.Fail,
                        StartDateTime = scheduledTask.LastStartUtc.Value
                    });
                }
                finally
                {
                    UpdateTask(scheduledTask);
                }
               
                return result;
            }
            return await new Task<bool>(() => false);
        }


       

        public IList<ScheduleTask> GetPendingTasks()
        {
            var now = DateTime.UtcNow;

            var query = from t in _taskRepository.GetAll()
                        where t.NextRunUtc.HasValue && t.NextRunUtc <= now && t.Enabled
                        orderby t.NextRunUtc
                        select t;

            return query.ToList();
        }

        public virtual bool HasRunningTasks()
        {
            var query = GetRunningTasksQuery();
            return query.Any();
        }

        public virtual bool IsTaskRunning(int taskId)
        {
            if (taskId <= 0)
                return false;

            var query = GetRunningTasksQuery();
            query = query.Where(t => t.Id == taskId);
            return query.Any();
        }

        public IList<ScheduleTask> GetRunningTasks()
        {
            return GetRunningTasksQuery().ToList();
        }

        private IQueryable<ScheduleTask> GetRunningTasksQuery()
        {
            var query = from t in _taskRepository.GetAll()
                        where t.LastStartUtc.HasValue && t.LastStartUtc.Value > (t.LastEndUtc ?? DateTime.MinValue)
                        orderby t.LastStartUtc
                        select t;

            return query;
        }


        public void CalculateFutureSchedules(IEnumerable<ScheduleTask> tasks, bool isAppStart = false)
        {
            foreach (var task in tasks)
            {
                task.NextRunUtc = GetNextSchedule(task);
                if (isAppStart)
                {
                    task.ProgressPercent = null;
                    task.ProgressMessage = null;
                    if (task.LastEndUtc.GetValueOrDefault() < task.LastStartUtc)
                    {
                        task.LastEndUtc = task.LastStartUtc;
                        task.LastError = "Abnormally aborted due to application shutdown";
                    }
                }
                UpdateTask(task);
            }
        }

        public DateTime? GetNextSchedule(ScheduleTask task)
        {
            if (task.Enabled)
            {
                try
                {
                    var baseTime = DateTime.UtcNow;
                    var next = CronExpression.GetNextSchedule(task.CronExpression, baseTime);
                    return next;
                }
                catch
                {
                    // ignored
                }
            }

            return null;
        }
    }
    }
