﻿using System;
using System.Collections.Generic;

namespace alfred.service.ScheduleTasks
{
    public class ScheduledTaskProvider: IScheduledTaskProvider
    {

        private readonly IDictionary<string, Func<IJob>> _factories;

        public ScheduledTaskProvider(IDictionary<string, Func<IJob>> factories)
        {
            if (factories == null)
                throw new ArgumentNullException("factories", "Task factories must be registered.");

            _factories = factories;
        }

        /// <summary>
        /// Gets an instance of the task for the specified key. Returns null
        /// if no task factory is registered for the key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IJob GetTask(string key)
        {
            var factory = _factories[key];
            var task = factory == null ? null : factory();
            return task;
        }
        
    }
}