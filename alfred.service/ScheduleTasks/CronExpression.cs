﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using alfred.service.Helpers;
using CronExpressionDescriptor;
using NCrontab;

namespace alfred.service.ScheduleTasks
{
    public static class CronExpression
    {

        public static bool IsValid(string expression)
        {
            try
            {
                CrontabSchedule.Parse(expression);
                return true;
            }
            catch
            {
                // ignored
            }

            return false;
        }

        public static DateTime GetNextSchedule(string expression, DateTime baseTime)
        {
            return GetFutureSchedules(expression, baseTime, 1).FirstOrDefault();
        }

        public static DateTime GetNextSchedule(string expression, DateTime baseTime, DateTime endTime)
        {
            return GetFutureSchedules(expression, baseTime, endTime, 1).FirstOrDefault();
        }

        public static IEnumerable<DateTime> GetFutureSchedules(string expression, DateTime baseTime, int max = 10)
        {
            return GetFutureSchedules(expression, baseTime, DateTime.MaxValue);
        }

        public static IEnumerable<DateTime> GetFutureSchedules(string expression, DateTime baseTime, DateTime endTime, int max = 10)
        {
            if (expression == null) throw new ArgumentNullException("expression");


            var schedule = CrontabSchedule.Parse(expression);
            return schedule.GetNextOccurrences(baseTime, endTime).Take(max);
        }

        public static string GetFriendlyDescription(string expression)
        {
            var options = new Options
            {
                DayOfWeekStartIndexZero = true,
                ThrowExceptionOnParseError = true,
                Verbose = false,
                Use24HourTimeFormat = string.IsNullOrEmpty(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.AMDesignator)
            };

            if (CommonHelper.HasValue(expression))
            {
                try
                {
                    return ExpressionDescriptor.GetDescription(expression, options);
                }
                catch
                {
                    // ignored
                }
            }

            return "?";
        }

    }
}