﻿using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Infrastructure;

namespace alfred.service.ScheduleTasks
{
    public class ScheduleTaskLogService : RepositoryService<ScheduleTaskLog>, IRepositoryService<ScheduleTaskLog>
    {

          public ScheduleTaskLogService(IScheduleTaskLogRepository scheduleTaskLogRepository, IUnitOfWork unitOfWork)
            : base(scheduleTaskLogRepository, unitOfWork)
        {
        }
    }
}
