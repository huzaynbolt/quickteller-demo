using System.Collections.Generic;
using alfred.service.Configurations;

namespace alfred.service.Messages
{
    public class MessageSetting:ISettings
    {
        public MessageSetting()
        {
            ActiveMessageSenderSystemNames = new List<string>();
        }
        public string DefaultEmailProvider { get; set; }
        public  string DefaultSMSProvider { get; set; }
        public List<string> ActiveMessageSenderSystemNames { get; set; }
    }
}