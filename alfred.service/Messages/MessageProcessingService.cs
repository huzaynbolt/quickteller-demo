using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.enums;

namespace alfred.service.Messages
{
    public class MessageProcessingService : IMessageProcessingService
    {
        private readonly IMessageSenderProvider _messageSenderProvider;
        private readonly MessageSetting _messageSetting;
        public MessageProcessingService(IMessageSenderProvider messageSenderProvider, MessageSetting options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));
            _messageSetting = options;
            _messageSenderProvider = messageSenderProvider ?? throw new ArgumentNullException(nameof(messageSenderProvider));
        }

        public IList<IMessageSender> LoadActiveMessageSenders()
        {
            return LoadAllMessageSenders().Where(x => _messageSetting.ActiveMessageSenderSystemNames.Contains(x.SystemName)).ToList();
        }

        public IMessageSender LoadMessageSenderBySystemName(string systemName)
        {
            return _messageSenderProvider.GetMessageSender(systemName);
        }

        public IList<IMessageSender> LoadAllMessageSenders()
        {
            return _messageSenderProvider.GetMessageSenders();
        }

        public NotificationType GetNotificationType(string messageSenderSystemName)
        {
            var messageSender = LoadMessageSenderBySystemName(messageSenderSystemName);
            if (messageSender == null)
                throw new Exception("Provider couldn't be loaded");
            return messageSender.NotificationType;
        }

        public ProcessMessageResult ProcessMessage(ProcessMessageRequest processMessageRequest)
        {
            var messageSender = LoadMessageSenderBySystemName(processMessageRequest.Provider);
            if (messageSender == null)
                throw new Exception("Provider couldn't be loaded");
            return messageSender.ProcessMessage(processMessageRequest);
        }

        public MessageSetting GetMessageSetting()
        {
            return _messageSetting;
        }
    }
}