using System.Collections.Generic;
using System.Threading.Tasks;

namespace alfred.service.Messages
{
    public interface IMailer
    {

        Task Send(string from, IList<string> to, string subject, string body, IList<EmailAttachment> emailAttachments );
        Task Send(ProcessMessageRequest viewModel);
    }
    
    public interface ISmsSender
    {
        Task Send(string to, string body, string from = null);
    }
}