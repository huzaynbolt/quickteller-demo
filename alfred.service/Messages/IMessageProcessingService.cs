using System.Collections.Generic;
using alfred.data.domain.enums;

namespace alfred.service.Messages
{
    public interface IMessageProcessingService
    {
        /// <summary>
        /// Load active Message Sender
        /// </summary>
        /// <returns>Message Senders</returns>
        IList<IMessageSender> LoadActiveMessageSenders();

        /// <summary>
        /// Load Message Sender by system name
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Found Message Sender</returns>
        IMessageSender LoadMessageSenderBySystemName(string systemName);

        /// <summary>
        /// Load all Message Senders
        /// </summary>
        /// <returns>Message Senders</returns>
        IList<IMessageSender> LoadAllMessageSenders();


        /// <summary>
        /// Gets a Message Sender type
        /// </summary>
        /// <param name="messageSendersSystemName">Message Sender system name</param>
        /// <returns>A payment method type</returns>
        NotificationType GetNotificationType(string messageSendersSystemName);


        /// <summary>
        /// Process a Message
        /// </summary>
        /// <param name="processMessageRequest">Message Required for processing</param>
        /// <returns>Process Message result</returns>
        ProcessMessageResult ProcessMessage(ProcessMessageRequest processMessageRequest);

        MessageSetting GetMessageSetting();

    }
}