using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;
using alfred.data.Repository;
using alfred.service.Helpers;
using alfred.service.Logging;

namespace alfred.service.Messages
{
    public class NotificationService : INotificationService
    {
        private readonly IMailer _mailer;
        private readonly IWebHelper _webHelper;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IQueuedNotificationRepository _queuedNotificationRepository;
        private readonly IMessageProcessingService _messageService;
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;

        private const string SenderEmail = "Alfred <noreply@alfred.ai>";
        public NotificationService(IMailer mailer,
            IWebHelper webHelper,
            IDateTimeHelper dateTimeHelper, IMessageProcessingService messageService, IQueuedNotificationRepository queuedNotificationRepository, ILogger logger, IUnitOfWork unitOfWork)
        {
            _mailer = mailer;
            _webHelper = webHelper;
            _dateTimeHelper = dateTimeHelper;
            _messageService = messageService;
            _queuedNotificationRepository = queuedNotificationRepository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task ResetPassword(string email, string userName, string callbackUrl)
        {
            var eb = new MessageBuilder();
            var body = GetEmailTemplate("PasswordRecoveryEmail.html");
            var placeHolders = new Dictionary<string, string>
            {
                {"${date}", _dateTimeHelper.ConvertToUserTime(DateTime.Now, DateTimeKind.Utc).ToString("D")},
                {"${FullName}", userName},
                {"${HostUrl}", _webHelper.GetHost()},
                {"${CallbackUrl}", callbackUrl}
            };
            var environment = ConfigurationManager.AppSettings["environment"];
            var testEnvironments = ConfigurationManager.AppSettings["testEnvironments"].Split(',');
            var subject = "Password Reset";
            if (testEnvironments.Contains(environment))
            {
                subject += string.Format(" - {0}", environment);
            }
            body = ParseEmail(placeHolders, body);
            eb.To(email)
                .From(SenderEmail)
                .WithSubject(subject)
                .WithBody(body);
            var emailToSend = eb.ToMessageRequest();
            await _mailer.Send(emailToSend);
        }



        public async Task NewAccount(string email, string userName, string role, string callbackUrl)
        {
            var eb = new MessageBuilder();
            var body = GetEmailTemplate("NewAccountEmail.html");
            var placeHolders = new Dictionary<string, string>
            {
                {"${date}", _dateTimeHelper.ConvertToUserTime(DateTime.Now, DateTimeKind.Utc).ToString("D")},
                {"${FullName}", userName},
                {"${UserName}", email},
                {"${HostUrl}", _webHelper.GetHost()},
                {"${CallbackUrl}", callbackUrl},
                {"${RoleName}", role}
            };
            var environment = ConfigurationManager.AppSettings["environment"];
            var testEnvironments = ConfigurationManager.AppSettings["testEnvironments"].Split(',');
            var subject = "Activate Account";
            if (testEnvironments.Contains(environment))
            {
                subject += string.Format(" - {0}", environment);
            }
            body = ParseEmail(placeHolders, body);
            eb.To(email)
                .From(SenderEmail)
                .WithSubject(subject)
                .WithBody(body);
            var emailToSend = eb.ToMessageRequest();
            await _mailer.Send(emailToSend);
        }

        public QueuedNotification GetById(long id)
        {
            return id <= 0 ? null : _queuedNotificationRepository.GetById(id);
        }

        public long SendNotification(string application, string url, string template, string provider, int priority, string @from,
            string subject, string body, IList<Param> @params, IList<FileAttachment> attachments, IList<string> to, IList<string> cc, IList<string> bcc,
            NotificationType type)
        {
             if (priority <= 0)
            {
                priority = 5;
            }
            if (string.IsNullOrEmpty(provider))
            {
                var setting = _messageService.GetMessageSetting();
                provider = type == NotificationType.Email ? setting.DefaultEmailProvider : setting.DefaultSMSProvider;
            }
            //Peter Makafan - GA <peter.makafan@globalaccelerex.com>
            if (cc == null)
            {
                cc = new List<string>();
            }

            if (bcc == null)
            {
                bcc = new List<string>();
            }
            if (to == null)
            {
                to = new List<string>();
            }

            if (@params == null)
            {
                @params = new List<Param>();
            }
            if (attachments == null)
            {
              attachments = new List<FileAttachment>();   
            }
            var parameters = new Dictionary<string, string>();
            foreach (var param in @params)
            {
                var key = "${" + param.Key + "}";
                if (!parameters.ContainsKey(key))
                {
                  parameters.Add(key, param.Value);   
                }
            }

            var messageBody = ParseEmail(parameters, body);
            var messageSubject = ParseEmail(parameters, subject);
           /* var templateEntity = _templateRepository.Get(x => x.Name == template);
            if (templateEntity != null)
            {
                messageBody = ParseString(parameters, templateEntity.Body);
                messageSubject = ParseString(parameters, templateEntity.Subject);
            }
            else
            {
               
            }*/
            var queuedNotification = new QueuedNotification()
            {
              EmailAttachments = new List<NotificationAttachment>(),
              Application = application,
              CreatedOn = DateTime.Now,
              Priority =  priority,
              DeliveryStatus = DeliveryStatus.Pending,
              NotificationType = type,
              Body =  messageBody,
              CallBackUrl = url,
              Provider = provider,
              From = from,
              Cc = string.Join(";", cc),
              Bcc = string.Join(";", bcc),
              To = string.Join(";", to),
              Subject =  messageSubject,
              
            };
            foreach (var attachment in attachments)
            {
                var emailAttachment = new NotificationAttachment()
                {
                    Content = attachment.Content,
                    ContentType = attachment.ContentType,
                    Name =  attachment.Name
                };
                queuedNotification.EmailAttachments.Add(emailAttachment);
            }
           
           Insert(queuedNotification);
          return  queuedNotification.Id;
        }

        public void Insert(QueuedNotification queuedNotification)
        {
            _queuedNotificationRepository.Add(queuedNotification);
           _unitOfWork.Commit();
        }

        public async Task<bool> SendEmail(QueuedNotification queuedNotification)
        {
            const int maxTries = 5;
            var result = false;
            try
            {
                
                var  to = String.IsNullOrWhiteSpace(queuedNotification.To) ? null : queuedNotification.To.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var bcc = String.IsNullOrWhiteSpace(queuedNotification.Bcc) ? null : queuedNotification.Bcc.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var cc = String.IsNullOrWhiteSpace(queuedNotification.Cc) ? null : queuedNotification.Cc.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                to = to ?? new string[]{};
                cc = cc ?? new string[] { };
                bcc = bcc ?? new string[] { };
                MessageBuilder messageBuilder = new MessageBuilder();
                messageBuilder
                    .UseProvider(queuedNotification.Provider)
                    .From(queuedNotification.From)
                    .AndTo(to)
                    .AndCc(cc)
                    .AndBcc(bcc)
                    .WithSubject(queuedNotification.Subject)
                    .WithBody(queuedNotification.Body);
                queuedNotification.EmailAttachments =
                    queuedNotification.EmailAttachments ?? new List<NotificationAttachment>();
                foreach (var emailAttachment in queuedNotification.EmailAttachments)
                {
                   var bytes = Convert.FromBase64String(emailAttachment.Content);
                    messageBuilder.WithAttachment(new EmailAttachment()
                    {
                        Name = emailAttachment.Name,
                        MimeType = emailAttachment.ContentType,
                        Data = bytes
                    });
                }
                var messageResult = _messageService.ProcessMessage(messageBuilder.ToMessageRequest());
                if (messageResult.Success && messageResult.NewDeliveryStatus == DeliveryStatus.Sent)
                {
                    queuedNotification.SentOn = DateTime.UtcNow;
                    queuedNotification.DeliveryStatus = DeliveryStatus.Sent;
                    result = true;
                }
                else
                {
                    queuedNotification.SentTries = queuedNotification.SentTries + 1;
                    if (queuedNotification.SentTries >= maxTries)
                    {
                        queuedNotification.DeliveryStatus = DeliveryStatus.Failed;
                    }
                    Update(queuedNotification);
                }
                
                return await Task.FromResult(result);
            }
            catch (Exception e)
            {
                _logger.Error("ErrorSendingEmail", e);
            }
            finally
            {
                queuedNotification.SentTries = queuedNotification.SentTries + 1;
                if (queuedNotification.SentTries >= maxTries)
                {
                    queuedNotification.DeliveryStatus = DeliveryStatus.Failed;
                }
                Update(queuedNotification);
            }
            return await Task.FromResult(result);
        }

        public void Update(QueuedNotification queuedNotification)
        {
            _queuedNotificationRepository.Update(queuedNotification);
            _unitOfWork.Commit();
        }

        public IPagedList<QueuedNotification> SearchEmails(string fromEmail, string toEmail, DateTime? startTime, DateTime? endTime,
            bool loadNotSentItemsOnly, int maxSendTries, bool loadNewest, int pageIndex, int pageSize)
        {
            fromEmail = (fromEmail ?? String.Empty).Trim();
            toEmail = (toEmail ?? String.Empty).Trim();

            var query = _queuedNotificationRepository.GetAll();

            if (!String.IsNullOrEmpty(fromEmail))
                query = query.Where(qe => qe.From.Contains(fromEmail));

            if (!String.IsNullOrEmpty(toEmail))
                query = query.Where(qe => qe.To.Contains(toEmail));

            if (startTime.HasValue)
                query = query.Where(qe => qe.CreatedOn >= startTime);

            if (endTime.HasValue)
                query = query.Where(qe => qe.CreatedOn <= endTime);

            if (loadNotSentItemsOnly)
                query = query.Where(qe => !qe.SentOn.HasValue);

            

            query = query.Where(qe => qe.SentTries < maxSendTries);

            query = query.OrderByDescending(qe => qe.Priority);

            query = loadNewest ?
                ((IOrderedQueryable<QueuedNotification>)query).ThenByDescending(qe => qe.CreatedOn) :
                ((IOrderedQueryable<QueuedNotification>)query).ThenBy(qe => qe.CreatedOn);

            var queuedEmails = new PagedResult<QueuedNotification>(query, pageIndex, pageSize);
            return queuedEmails;
        }

        public IMessageSender GetProvider(string provider)
        {
            return _messageService.LoadMessageSenderBySystemName(provider);
        }


        #region Helpers
        /// <summary>
        ///  Parse an email Body using the place holders ${item}
        /// </summary>
        /// <param name="placeHolders"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        private static string ParseEmail(Dictionary<string, string> placeHolders, string body)
        {
            return placeHolders.Keys.Where(key => Regex.IsMatch(key, @"\${[^""]+}")).Aggregate(body, (current, key) => current.Replace(key, placeHolders[key]));
        }

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private string GetEmailTemplate(string fileName)
        {
            var filePath = _webHelper.MapPath("/Views/EmailTemplate/");
            var emailTemplate = Path.Combine(filePath, fileName);
            var templateContent = string.Empty;

            if (File.Exists(emailTemplate))
            {
                try
                {
                    StreamReader stream;
                    using (stream = new StreamReader(emailTemplate))
                    {
                        templateContent = stream.ReadToEnd();
                        return templateContent;
                    }
                }
                catch (IOException)
                {

                }
            }

            return templateContent;
        }
        #endregion
    }

    public class FileAttachment
    {
        public string Content { get; set; }
        public string ContentType { get; set; }
        public string Name { get; set; }
    }
}