using System.Collections.Generic;

namespace alfred.service.Messages
{
    public interface IMessageSenderProvider
    {
        IList<IMessageSender> GetMessageSenders();
        IMessageSender GetMessageSender(string systemName);
    }
}