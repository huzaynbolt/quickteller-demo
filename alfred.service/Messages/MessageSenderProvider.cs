using System;
using System.Collections.Generic;
using System.Linq;

namespace alfred.service.Messages
{
    public class MessageSenderProvider : IMessageSenderProvider
    {
        private readonly IDictionary<string, Func<IMessageSender>> _factories;

        public MessageSenderProvider(IDictionary<string, Func<IMessageSender>> factories)
        {
            _factories = factories;
        }

        public IList<IMessageSender> GetMessageSenders()
        {
            return _factories.Values.Select(f => f()).ToList();
        }

        public IMessageSender GetMessageSender(string systemName)
        {

            if (string.IsNullOrEmpty(systemName))
                return null;
            var factory = _factories[systemName];
            var  messageSender = factory == null ? null : factory();
            return messageSender;
        }
    }
}