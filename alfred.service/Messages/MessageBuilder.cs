﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;

namespace alfred.service.Messages
{
    public class MessageBuilder
    {
        private readonly ProcessMessageRequest _viewModel = new ProcessMessageRequest();

       
        public MessageBuilder WithBody(IEnumerable<KeyValuePair<string, IList>> lists)
        {
            var writer = new StringWriter();
            var html = new HtmlTextWriter(writer);

            html.RenderBeginTag(HtmlTextWriterTag.H1);
            html.WriteEncodedText(_viewModel.Subject);
            html.RenderEndTag();
            html.WriteBreak();
            html.WriteBreak();
            foreach (var list in lists)
            {
                html.RenderBeginTag(HtmlTextWriterTag.H3);
                html.WriteEncodedText(list.Key);
                html.RenderEndTag();
                html.RenderBeginTag(HtmlTextWriterTag.Ul);
                foreach (var item in list.Value)
                {
                    html.RenderBeginTag(HtmlTextWriterTag.Li);
                    html.WriteEncodedText(item.ToString());
                    html.RenderEndTag();
                }
                html.RenderEndTag();
                html.WriteBreak();
                html.WriteBreak();
            }

            _viewModel.Body = writer.ToString();

            return this;
        }

       public MessageBuilder From(string from)
        {
            _viewModel.From = from;
            return this;
        }

        public MessageBuilder To(string to)
        {
            _viewModel.To = new List<string> { to };
            return this;
        }

        public MessageBuilder To(IList<string> to)
        {
            _viewModel.To = new List<string>();
            _viewModel.To.AddRange(to);
            return this;
        }

        public MessageBuilder AndTo(string to)
        {
            _viewModel.To = _viewModel.To ?? new List<string>();
            _viewModel.To.Add(to);
            return this;
        }

        public MessageBuilder AndTo(IList<string> to)
        {
            _viewModel.To = _viewModel.To ?? new List<string>();
            _viewModel.To.AddRange(to);
            return this;
        }

        public MessageBuilder WithSubject(string subject)
        {
            _viewModel.Subject = subject;
            return this;
        }

        public MessageBuilder WithBody(string body)
        {
            _viewModel.Body = body;
            return this;
        }

        

        public ProcessMessageRequest ToMessageRequest()
        {
            return _viewModel;
        }

        public MessageBuilder WithAttachments(IList<EmailAttachment> attachments)
        {
            _viewModel.Attachments = _viewModel.Attachments ?? new List<EmailAttachment>();
            _viewModel.Attachments.AddRange(attachments);
            return this;
        }

        public MessageBuilder WithAttachment(EmailAttachment attachment)
        {
            _viewModel.Attachments = _viewModel.Attachments ?? new List<EmailAttachment>();
            _viewModel.Attachments.Add(attachment);
            return this;
        }

        public MessageBuilder UseProvider(string provider)
        {
            _viewModel.Provider = provider;
            return this;
        }

        public MessageBuilder AndCc(IList<string> cc)
        {
            _viewModel.Cc = _viewModel.Cc ?? new List<string>();
            _viewModel.Cc.AddRange(cc);
            return this;
        }

        public MessageBuilder AndBcc(IList<string> bcc)
        {
            _viewModel.Bcc = _viewModel.Bcc ?? new List<string>();
            _viewModel.Bcc.AddRange(bcc);
            return this;
        }
    }
}