

using System.Collections.Generic;
using alfred.data.domain.enums;

namespace alfred.service.Messages
{
    public class ProcessMessageResult
    {
        private  DeliveryStatus  _newDeliveryStatus =  DeliveryStatus.Pending;
        public IList<string> Errors { get; set; }

        public ProcessMessageResult()
        {
            Errors = new List<string>();
        }

        public bool Success
        {
            get { return Errors.Count == 0; }
        }

        public void AddError(string error)
        {
            Errors.Add(error);
        }
        /// <summary>
        /// Gets or sets a payment status after processing
        /// </summary>
        public DeliveryStatus NewDeliveryStatus
        {
            get
            {
                return  _newDeliveryStatus;
            }
            set
            {
                _newDeliveryStatus = value;
            }
        }

       
    }
}