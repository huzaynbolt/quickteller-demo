using alfred.data.domain.enums;

namespace alfred.service.Messages
{
    public interface IMessageSender
    {
        /// <summary>
        /// Gets a Notification type
        /// </summary>
        NotificationType NotificationType { get; }

        string Description { get; }
        string FriendlyName { get; }
        string SystemName { get; }
        /// <summary>
        /// Send a message
        /// </summary>
        /// <param name="messageRequest"></param>
        ProcessMessageResult ProcessMessage(ProcessMessageRequest messageRequest);
    }
}