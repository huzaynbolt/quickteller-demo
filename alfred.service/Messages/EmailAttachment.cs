﻿namespace alfred.service.Messages
{
    public class EmailAttachment
    {
        /// <summary>
        /// The attachment's binary data (only applicable if location is <c>Blob</c>)
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// The attachment file name (without path)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The attachment file's mime type, e.g. <c>application/pdf</c>
        /// </summary>
        public string MimeType { get; set; }
    }
}