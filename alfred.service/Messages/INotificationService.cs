﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.data.Infrastructure;

namespace alfred.service.Messages
{
    public interface INotificationService
    {
        Task ResetPassword(string email, string userName, string callbackUrl);

        Task NewAccount(string email, string userName, string role, string callbackUrl);

        QueuedNotification GetById(long id);

        long SendNotification(string application, string url,
            string template, string provider, int priority,
            string from, string subject, string body, IList<Param> @params,
            IList<FileAttachment> attachments, IList<string> to, IList<string> cc, IList<string> bcc, NotificationType type
        );

        void Insert(QueuedNotification queuedNotification);

        /// <summary>
        /// Sends a queued message
        /// </summary>
        /// <param name="queuedNotification">Queued message</param>
        /// <returns>Whether the operation succeeded</returns>
        Task<bool>  SendEmail(QueuedNotification queuedNotification);




        /// <summary>
        /// Search queued emails
        /// </summary>
        /// <param name="fromEmail">From Email</param>
        /// <param name="toEmail">To Email</param>
        /// <param name="startTime">The start time</param>
        /// <param name="endTime">The end time</param>
        /// <param name="loadNotSentItemsOnly">A value indicating whether to load only not sent emails</param>
        /// <param name="maxSendTries">Maximum send tries</param>
        /// <param name="loadNewest">A value indicating whether we should sort queued email descending; otherwise, ascending.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Email item collection</returns>
        IPagedList<QueuedNotification> SearchEmails(string fromEmail,
            string toEmail, DateTime? startTime, DateTime? endTime,
            bool loadNotSentItemsOnly, int maxSendTries,
            bool loadNewest, int pageIndex, int pageSize);

        IMessageSender GetProvider(string provider);
    }
}