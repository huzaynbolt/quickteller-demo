﻿namespace alfred.service.Messages
{
    public class Param
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}