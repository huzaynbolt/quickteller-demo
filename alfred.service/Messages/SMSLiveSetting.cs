using alfred.service.Configurations;

namespace alfred.service.Messages
{
    public class SMSLiveSetting:ISettings
    {
        public string Username { get; set; } 
        public string Password { get; set; }
        public string SmsGatewaySubAccount{ get; set; }
        public string GatewayUrl { get; set; }
    }
}