using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using alfred.data.domain.enums;
using Microsoft.AspNet.Identity;

namespace alfred.service.Messages
{
    public class SmtpMailer : IIdentityMessageService, IMessageSender, IMailer
    {
        private readonly SmtpSetting _setting;

        public SmtpMailer(SmtpSetting setting)
        {
            _setting = setting;
        }

        public System.Threading.Tasks.Task SendAsync(IdentityMessage message)
        {
            if (message == null) throw new ArgumentNullException("message");
            // Plug in your email service here to send an email.
           return Send(string.Empty, message.Destination, message.Subject, message.Body);
        }

        public async System.Threading.Tasks.Task Send(string from, IList<string> to, string subject, string body,IList<EmailAttachment> emailAttachments )
        {
            if (string.IsNullOrWhiteSpace(from))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "from");
            }
            if (!to.Any())
            {
                throw new ArgumentException("must provide at least one recipient address", "to");
            }
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "subject");
            }
            if (string.IsNullOrWhiteSpace(body))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "body");
            }
            emailAttachments = emailAttachments ?? new List<EmailAttachment>();
            // Create the mail message
            using (var smtpClient = new SmtpClient(_setting.Host, _setting.Port))
            {
                try
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.EnableSsl = _setting.EnableSsl;
                    smtpClient.Credentials = new NetworkCredential(_setting.UserName, _setting.Password);
                    using (var mailMessage = new MailMessage { From = new MailAddress(from), Subject = subject, Body = body })
                    {
                        foreach (var address in to)
                            mailMessage.To.Add(address);
                        foreach (var attachement in emailAttachments)
                        {
                            mailMessage.Attachments.Add(new Attachment(new MemoryStream(attachement.Data),attachement.Name, attachement.MimeType));
                        }

                        mailMessage.BodyEncoding = Encoding.UTF8;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        // New up the SMTP client
                        smtpClient.Send(mailMessage);
                    }
                    
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    throw;
                }
            }
           
        }

        public async System.Threading.Tasks.Task Send(string from, string to, string subject, string body)
        {
            await Send(from, new[] { to }, subject, body, null);
        }

        public async System.Threading.Tasks.Task Send(ProcessMessageRequest viewModel)
        {
            await Send(viewModel.From, viewModel.To, viewModel.Subject, viewModel.Body, viewModel.Attachments);
        }


         public NotificationType NotificationType => NotificationType.Email;
        public string Description => "Simple Mail Transfer Protocol";
        public string FriendlyName => "Simple Mail Transfer Protocol";
        public string SystemName => "SMTPMessageSender";

        public ProcessMessageResult ProcessMessage(ProcessMessageRequest messageRequest)
        {
           return Send(messageRequest.From, messageRequest.To, messageRequest.Cc, messageRequest.Bcc, messageRequest.Subject, messageRequest.Body, messageRequest.Attachments);
        }

        private ProcessMessageResult Send(string from,
            List<string> to,
            List<string> cc,
            List<string> bcc,
            string subject,
            string body,
            List<EmailAttachment> emailAttachments)
        {
            ProcessMessageResult result = new ProcessMessageResult();
            if (string.IsNullOrWhiteSpace(from))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "from");
            }
            if (!to.Any())
            {
                throw new ArgumentException("must provide at least one recipient address", "to");
            }
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "subject");
            }
            if (string.IsNullOrWhiteSpace(body))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "body");
            }
            emailAttachments = emailAttachments ?? new List<EmailAttachment>();
            from = string.Format("{0} <{1}>", from, _setting.From);
            using (var smtpClient = new SmtpClient(_setting.Host, _setting.Port))
            {
               
                try
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.EnableSsl = _setting.EnableSsl;
                    smtpClient.Credentials = new NetworkCredential(_setting.UserName, _setting.Password);
                    using (var mailMessage = new MailMessage { From = new MailAddress(from), Subject = subject, Body = body })
                    {
                        foreach (var address in to)
                            mailMessage.To.Add(address);
                        if (cc != null)
                            foreach (var address in cc)
                            {
                                mailMessage.CC.Add(address);
                            }
                        if (bcc != null)
                            foreach (var address in bcc)
                            {
                                mailMessage.Bcc.Add(address);
                            }
                        foreach (var attachement in emailAttachments)
                        {
                            mailMessage.Attachments.Add(new Attachment(new MemoryStream(attachement.Data), attachement.Name, attachement.MimeType));
                        }

                        mailMessage.BodyEncoding = Encoding.UTF8;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        // New up the SMTP client
                        smtpClient.Send(mailMessage);
                        result.NewDeliveryStatus = DeliveryStatus.Sent;
                    }
                   

                }
                catch (Exception ex)
                {
                   // _logger.LogError(1002, ex, "SMTP.Send");
                    while (ex != null)
                    {
                        result.AddError(ex.Message);
                        ex = ex.InnerException;
                    }
                }
            }
          
            return result;
        }
    }
}