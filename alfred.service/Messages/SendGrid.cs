using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using alfred.data.domain.enums;
using alfred.service.Configurations;
using alfred.service.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace alfred.service.Messages
{
    public  class SendGrid : IMessageSender
    {
        private readonly SendGridSetting _sendGridSetting;
        private readonly ILogger _logger;

        public SendGrid(SendGridSetting sendGridSetting, ILogger logger)
        {
            _sendGridSetting = sendGridSetting;
            _logger = logger;
        }

        public NotificationType NotificationType { get{return  NotificationType.Email;} }
        public string Description { get { return "SendGrid"; } }
        public string FriendlyName { get { return "SendGrid"; } }
        public string SystemName { get { return "SendGrid"; } }
        public ProcessMessageResult ProcessMessage(ProcessMessageRequest messageRequest)
        {
            var result = new ProcessMessageResult();
            if (string.IsNullOrWhiteSpace(messageRequest.From))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "from");
            }
            if (!messageRequest.To.Any())
            {
                throw new ArgumentException("must provide at least one recipient address", "to");
            }
            if (string.IsNullOrWhiteSpace(messageRequest.Subject))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "subject");
            }
            if (string.IsNullOrWhiteSpace(messageRequest.Body))
            {
                throw new ArgumentException("cannot be null, empty, or white space", "body");
            }
            messageRequest.Attachments = messageRequest.Attachments ?? new List<EmailAttachment>();
            try
            {
                SendGridClient client = new SendGridClient(_sendGridSetting.ApiKey);
                var sendGridMessage = new SendGridMessage()
                {
                    From = new EmailAddress(messageRequest.From),
                    HtmlContent = messageRequest.Body,
                    Subject = messageRequest.Subject,
                    
                };
                foreach (var to in messageRequest.To)
                {
                    sendGridMessage.AddTo(to);
                }
                foreach (var to in messageRequest.Bcc)
                {
                    sendGridMessage.AddBcc(to);
                }
                foreach (var to in messageRequest.Cc)
                {
                    sendGridMessage.AddBcc(to);
                }
                foreach (var attachment in messageRequest.Attachments)
                {
                    var base64String = Convert.ToBase64String(attachment.Data);
                    sendGridMessage.AddAttachment(attachment.Name, base64String, attachment.MimeType);
                }
                var response = client.SendEmailAsync(sendGridMessage).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.NewDeliveryStatus = DeliveryStatus.Sent;
                }
                else
                {
                    result.AddError("Failed");
                    result.NewDeliveryStatus = DeliveryStatus.Failed;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                while (e != null)
                {
                    result.AddError(e.Message);
                    e = e.InnerException;

                }
            }
            return result;
        }
    }

    public class SendGridSetting:ISettings
    {
        public string ApiKey { get; set; }
    }
}