using System;
using System.Net.Http;
using System.Threading.Tasks;
using alfred.data.domain.enums;
using alfred.service.Logging;
using Microsoft.AspNet.Identity;

namespace alfred.service.Messages
{
    public class SmsService : IIdentityMessageService, IMessageSender, ISmsSender
    {
        //private const string GatewayUrl = "http://www.smslive247.com/http/index.aspx?cmd=sendquickmsg&"; //"http://www.estoresms.com/smsapi.php?";
       
       
        string msgType = "0";
        private readonly SMSLiveSetting _smsLiveSetting;
        private readonly ILogger _logger;


        public SmsService(SMSLiveSetting smsLiveSetting, ILogger logger)
        {
            _smsLiveSetting = smsLiveSetting;
            _logger = logger;
        }


        public async Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            await Task.FromResult(0);
        }

        public async Task Send(string to, string body, string @from = null)
        {
            string req = string.Format("{0}owneremail={1}&subacct={2}&subacctpwd={3}&message={4}&sender={5}&sendto={6}&msgtype={7}",
                _smsLiveSetting.GatewayUrl,
                _smsLiveSetting.Username,
                _smsLiveSetting.SmsGatewaySubAccount,
                _smsLiveSetting.Password,
                body,
                from,
                to,
                msgType
            );

            try
            {
                using (HttpClient client = new HttpClient())
                {

                 HttpResponseMessage responseMessage =   await client.PostAsync(req, new StringContent("PayLoad"));
                 var readAsStringAsync =  await responseMessage.Content.ReadAsStringAsync();
                 _logger.Information(string.Format("(To: {0})  : (Response: {1})",to, readAsStringAsync));
                }
            }
            catch (Exception  e)
            {
                _logger.Error(e.Message,e);
                //ignore
            }
           


            await  Task.FromResult(0);
        }

        public NotificationType NotificationType { get { return NotificationType.SMS; } }
        public string Description => "SMS Live GateWay";
        public string FriendlyName => "SMS Live GateWay";
        public string SystemName => "SMSLive";
        public ProcessMessageResult ProcessMessage(ProcessMessageRequest messageRequest)
        {
            ProcessMessageResult processMessageResult = new ProcessMessageResult();

            string to = string.Join(",", messageRequest.To);
            string req = string.Format("{0}owneremail={1}&subacct={2}&subacctpwd={3}&message={4}&sender={5}&sendto={6}&msgtype={7}",
                _smsLiveSetting.GatewayUrl,
                _smsLiveSetting.Username,
                _smsLiveSetting.SmsGatewaySubAccount,
                _smsLiveSetting.Password,
                messageRequest.Body,
                messageRequest.From,
                to,
                msgType
            );

            try
            {
                using (HttpClient client = new HttpClient())
                {

                    HttpResponseMessage responseMessage =  client.PostAsync(req, new StringContent("PayLoad")).Result;
                    var readAsStringAsync =  responseMessage.Content.ReadAsStringAsync().Result;
                    processMessageResult.NewDeliveryStatus  = DeliveryStatus.Sent;
                    _logger.Information(string.Format("(To: {0})   (Response: {1})", to, readAsStringAsync));
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception.Message, exception);
                //ignore
                while (exception != null)
                {
                    processMessageResult.AddError(exception.Message);
                    exception = exception.InnerException;
                }
            }
            return processMessageResult;
        }
    }
}