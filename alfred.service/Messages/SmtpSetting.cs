using alfred.service.Configurations;

namespace alfred.service.Messages
{
    public class SmtpSetting: ISettings
    {
        public string From { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
    }
}