using System.Collections.Generic;

namespace alfred.service.Messages
{
    public class ProcessMessageRequest
    {
        public ProcessMessageRequest()
        {
            To = new List<string>();
            Cc = new List<string>();
            Bcc = new List<string>();
            Attachments = new List<EmailAttachment>();
        }

        public string Provider { get; set; }
        public string From { get; set; }

        public List<string> To { get; set; }

        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public List<EmailAttachment> Attachments { get; set; }
       
    }
}