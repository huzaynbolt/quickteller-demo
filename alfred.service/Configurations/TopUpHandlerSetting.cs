﻿namespace alfred.service.Configurations
{
    public class TopUpHandlerSetting : ISettings
    {
        public string SignUpMessageContent { get; set; }
        public string SenderId { get; set; }
        public string TopUpMessageTemplate { get; set; }
    }


    public class TimedTopUpHandlerSetting : ISettings
    {
        public string SignUpMessageContent { get; set; }
        public string SenderId { get; set; }
        public string TopUpMessageTemplate { get; set; }
        public double ExpireAfter { get; set; }
    }
}