﻿namespace alfred.service.Configurations
{
    public class WalletSetting:ISettings
    {
        public string PurchaseEndPoint { get; set; }
        public string SecretKey { get; set; }
        public string PublicKey { get; set; }
    }
}