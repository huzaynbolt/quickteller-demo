﻿namespace alfred.service.Configurations
{
    public class GeneralSetting : ISettings
    {
        public decimal SignUpFee { get; set; }

        public decimal CustomerBalancePercent { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public string Url { get; set; }

        public string EmailProviderSystemName { get; set; }
        public string SMSProviderSystemName { get; set; }
        public string NotificationProvider { get; set; }
        public string SiteName { get; set; }
        public string SiteColor { get; set; }
    }
}