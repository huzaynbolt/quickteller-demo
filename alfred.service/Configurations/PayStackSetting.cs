﻿namespace alfred.service.Configurations
{
    public class PayStackSetting : ISettings
    {
        public string SecretKey { get; set; }
        public string PublicKey { get; set; }
        public bool UseSandBox { get; set; }

        public string CallbackUrl { get; set; }
        public string WebhookUrl { get; set; }

        public decimal HandlingFee { get; set; }

    }
}