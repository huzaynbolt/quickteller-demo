﻿namespace alfred.service.Configurations
{
    public class FunnelSettings : ISettings
    {
        public int EndPointMethod { get; set; }
        public string Url { get; set; }
        public  int RunAfter { get; set; }
        public string SenderId { get; set; }
        public string EmailSenderId { get; set; }
    }
}