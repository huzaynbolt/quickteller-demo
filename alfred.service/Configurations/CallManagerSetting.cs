﻿using alfred.data.domain.enums;

namespace alfred.service.Configurations
{
    public class CallManagerSetting : ISettings
    {
       
        public string AccountSid { get; set; }
        public string PhoneNumber { get; set; }
        public string AuthToken { get; set; }

        public int CallDuration { get; set; }

        public string CallScript { get; set; }

        public string Url { get; set; }

        public int RunAfter  { get; set; }

        public int NumberOfRetries { get; set; }
        public bool RecordCall { get; set; }
        public string NotificationUrl { get; set; }
       
        public string CallBackUrl { get; set; }
        public string ClientUrl { get; set; }
    }
}