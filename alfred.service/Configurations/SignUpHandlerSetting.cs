﻿namespace alfred.service.Configurations
{
    public class SignUpHandlerSetting: ISettings
    {
       
        public string SignUpMessageContent { get; set; }
        public string SenderId { get; set; }
        public bool SendMessage { get; set; }
    }
}