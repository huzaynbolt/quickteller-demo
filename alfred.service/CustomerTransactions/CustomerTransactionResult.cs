﻿using System.Collections.Generic;
using System.Linq;


namespace alfred.service.CustomerTransactions
{
    public class CustomerTransactionResult
    {

        /// <summary>
        /// Ctor
        /// </summary>
        public CustomerTransactionResult()
        {
            Errors = new List<string>();
        }

        /// <summary>
        /// Gets a value indicating whether request has been completed successfully
        /// </summary>
        public bool Success
        {
            get { return (!Errors.Any()); }
        }

        /// <summary>
        /// Add error
        /// </summary>
        /// <param name="error">Error</param>
        public void AddError(string error)
        {
            Errors.Add(error);
        }

        /// <summary>
        /// Errors
        /// </summary>
        public List<string> Errors { get; set; }

        /// <summary>
        /// Gets or sets the placed order
        /// </summary>
        public data.domain.entities.CustomerTransaction PlacedOrder { get; set; }
    }
}