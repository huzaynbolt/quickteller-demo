﻿using System;
using System.Collections.Generic;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;

namespace alfred.service.CustomerTransactions
{
    public interface ICustomerTransactionService
    {
        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <returns>Order</returns>
        CustomerTransaction GetById(long id);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customTrackingNumber">The custom order number</param>
        /// <returns>Order</returns>
        CustomerTransaction GetByCustomTrackingNumber(string customTrackingNumber);

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <returns>Order</returns>
        IList<CustomerTransaction> Get(long[] ids);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <returns>Order</returns>
        CustomerTransaction GetByGuid(Guid shipmentGuid);

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="shipment">The order</param>
        void Delete(CustomerTransaction shipment);

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Orders</returns>
        IPagedList<CustomerTransaction> Search(long customerId = 0,
            string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> psIds = null,
        
             int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="shipment">Order</param>
        void Insert(CustomerTransaction shipment);

        /// <summary>
        /// Updates the order
        /// </summary>
        void Update(CustomerTransaction shipment);

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        CustomerTransaction GetByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId, string paymentMethodSystemName);


        IPagedList<CustomerTransaction> Find(long institutionId = 0,
        
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> psIds = null,
            List<int> dsIds = null,
            string paymentRef = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        #endregion


        void InsertRecurringPayment(RecurringPayment rp);
        void UpdateRecurringPayment(RecurringPayment rp);

        IList<RecurringPayment> GetPendingRecurringPayments();
        RecurringPayment GetPendingRecurringPaymentById(long eventRecurringPaymentId);

        IPagedList<RecurringPayment> SearchRecurringPayments(
            long institutionId = 0,

            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);
    }
}