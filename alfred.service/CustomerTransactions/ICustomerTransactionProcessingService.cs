﻿using alfred.data.domain.entities;
using alfred.service.Payments;

namespace alfred.service.CustomerTransactions
{
    public interface ICustomerTransactionProcessingService
    {
        bool IsPaymentWorkflowRequired(CustomerTransaction shipment);
        CustomerTransactionResult PlaceOrder(ProcessPaymentRequest processPaymentRequest);
        
        bool CanCancelShipment(CustomerTransaction shipment);
        bool CanCapture(CustomerTransaction shipment);
        bool CanMarkShipmentAsPaid(CustomerTransaction shipment);
        bool CanRefund(CustomerTransaction shipment);
        bool CanRefundOffline(CustomerTransaction shipment);
        bool CanPartiallyRefund(CustomerTransaction shipment, decimal zero);
        bool CanPartiallyRefundOffline(CustomerTransaction shipment, decimal zero);
        bool CanVoid(CustomerTransaction shipment);
        bool CanVoidOffline(CustomerTransaction shipment);
        void MarkShipmentAsPaid(CustomerTransaction shipment);
        void ChangeStatus(CustomerTransaction shipment);
        void DeliverShipment(CustomerTransaction shipment);
        void RefundOffline(CustomerTransaction customerTransaction);




    }
}