﻿using System;
using alfred.data.domain.entities;
using alfred.data.domain.enums;
using alfred.service.Logging;
using alfred.service.Payments;

namespace alfred.service.CustomerTransactions
{
    public class CustomerTransactionProcessingService : ICustomerTransactionProcessingService
    {
        private readonly ICustomerTransactionService _shipmentService;
        private readonly IPaymentService _paymentService;
        private readonly ILogger _logger;
       
        public CustomerTransactionProcessingService(
            ICustomerTransactionService shipmentService,
            IPaymentService paymentService,
            ILogger logger
            )
        {
            _shipmentService = shipmentService;
            _paymentService = paymentService;
            _logger = logger;
           
        }

        public bool IsPaymentWorkflowRequired(CustomerTransaction shipment)
        {
            return true;
        }

       


        public CustomerTransactionResult PlaceOrder(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest == null)
                throw new ArgumentNullException("processPaymentRequest");
            CustomerTransaction shipment = _shipmentService.GetById(processPaymentRequest.ShipmentId);
            if (shipment == null)
            {
                throw new Exception("CustomerTransaction Not Found");
            }

            if (processPaymentRequest.ShipmentGuid == Guid.Empty)
                processPaymentRequest.ShipmentGuid = Guid.NewGuid();

            var result = new CustomerTransactionResult();
            try
            {
                #region 





                #endregion

                #region Addresses & pre payment workflow

                // give payment processor the opportunity to fullfill billing address
                var preProcessPaymentResult = _paymentService.PreProcessPayment(processPaymentRequest);

                if (!preProcessPaymentResult.Success)
                {
                    result.Errors.AddRange(preProcessPaymentResult.Errors);
                    result.Errors.Add("Common.Error.PreProcessPayment");
                    return result;
                }


                #endregion

                #region Payment workflow

                // skip payment workflow if shipment total equals zero
                bool skipPaymentWorkflow = processPaymentRequest.AmountTotal == decimal.Zero;

                // payment workflow
                if (!skipPaymentWorkflow)
                {
                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        throw new Exception(("Payment.CouldNotLoadMethod"));
                }
                else
                {
                    processPaymentRequest.PaymentMethodSystemName = "";
                }

                // process payment
                // var processPaymentResult = !skipPaymentWorkflow ? _paymentService.ProcessPayment(processPaymentRequest) : new ProcessPaymentResult { NewPaymentStatus = PaymentStatus.Paid };
                #region Payment workflow


                //process payment
                ProcessPaymentResult processPaymentResult = null;
                //skip payment workflow if order total equals zero
               
                if (!skipPaymentWorkflow)
                {
                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(processPaymentRequest.PaymentMethodSystemName);
                    if (paymentMethod == null)
                        throw new Exception("Payment method couldn't be loaded");

                    ////ensure that payment method is active
                    //if (!paymentMethod.IsPaymentMethodActive(_paymentSettings))
                    //    throw new Exception("Payment method is not active");

                    if (processPaymentRequest.HasAuthToken)
                    {
                        //recurring cart
                        switch (_paymentService.GetRecurringPaymentType(processPaymentRequest.PaymentMethodSystemName))
                        {
                            case RecurringPaymentType.NotSupported:
                                throw new Exception("Recurring payments are not supported by selected payment method");
                            case RecurringPaymentType.Manual:
                            case RecurringPaymentType.Automatic:
                                processPaymentResult = _paymentService.ProcessRecurringPayment(processPaymentRequest);
                                break;
                            default:
                                throw new Exception("Not supported recurring payment type");
                        }
                    }
                    else
                        //standard cart
                        processPaymentResult = _paymentService.ProcessPayment(processPaymentRequest);
                }
                else
                    //payment is not required
                    processPaymentResult = new ProcessPaymentResult { NewPaymentStatus = PaymentStatus.Paid };

                if (processPaymentResult == null)
                    throw new Exception("processPaymentResult is not available");

                #endregion
                #endregion

                if (processPaymentResult.Success)
                {
                    //recurring orders
                    if (processPaymentRequest.IsRecurring)
                    {
                        //create recurring payment (the first payment)
                        var rp = new RecurringPayment
                        {
                            
                            CyclePeriod = processPaymentRequest.RecurringCyclePeriod,
                            Interval = processPaymentRequest.RecurringInterval,
                            StartDateUtc = processPaymentRequest.RecurringStartDate,
                            IsActive = true,
                            CreatedOnUtc = DateTime.UtcNow,
                            InitialCustomerTransactionId = shipment.Id,
                            
                        };
                        rp.NextRunDate = rp.NextPaymentDate;
                        _shipmentService.InsertRecurringPayment(rp);

                        switch (_paymentService.GetRecurringPaymentType(processPaymentRequest.PaymentMethodSystemName))
                        {
                            case RecurringPaymentType.NotSupported:
                                //not supported
                                break;
                            case RecurringPaymentType.Manual:
                                rp.RecurringPaymentHistory.Add(new RecurringPaymentHistory
                                {
                                    RecurringPayment = rp,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    CustomerTransactionId = shipment.Id,
                                });
                                rp.NextRunDate = rp.NextPaymentDate;
                                _shipmentService.UpdateRecurringPayment(rp);
                                break;
                            case RecurringPaymentType.Automatic:
                                //will be created later (process is automated)
                                break;
                            default:
                                break;
                        }
                    }

                    #region Save details

                    shipment.PaymentMethodSystemName = processPaymentRequest.PaymentMethodSystemName;
                    shipment.AuthorizationTransactionId = processPaymentResult.AuthorizationTransactionId;
                    shipment.AuthorizationTransactionCode = processPaymentResult.AuthorizationTransactionCode;
                    shipment.CardCvv2 = processPaymentRequest.CreditCardCvv2;
                    shipment.CardType = processPaymentRequest.CreditCardType;
                    shipment.CardExpirationMonth = processPaymentRequest.CreditCardExpireMonth.ToString("D2");
                    shipment.CardExpirationYear = processPaymentRequest.CreditCardExpireYear.ToString();
                    // shipment.Email = processPaymentRequest.Beneficiary;
                    shipment.CardName = processPaymentRequest.CreditCardName;
                    shipment.CardNumber = processPaymentRequest.CreditCardNumber;

                    shipment.PaidDateUtc = DateTime.Now;
                    shipment.AuthorizationTransactionResult = processPaymentResult.AuthorizationTransactionResult;
                    shipment.PaymentStatus = processPaymentResult.NewPaymentStatus;


                    _shipmentService.Update(shipment);
                    result.PlacedOrder = shipment;

                    #endregion
                }
                else
                {
                    result.AddError("Payment.PayingFailed");

                    foreach (var paymentError in processPaymentResult.Errors)
                    {
                        result.AddError(paymentError);
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc);
                result.AddError(exc.Message);
            }

            if (result.Errors.Count > 0)
            {
                _logger.Error(string.Join(" ", result.Errors));
            }

            return result;


        }

        

        public bool CanCancelShipment(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            

            return false;
        }

        public bool CanCapture(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            

            if (shipment.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportCapture(shipment.PaymentMethodSystemName))
                return true;

            return false;
        }

        public bool CanMarkShipmentAsPaid(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

          

            if (shipment.PaymentStatus == PaymentStatus.Paid ||
                shipment.PaymentStatus == PaymentStatus.Refunded ||
                shipment.PaymentStatus == PaymentStatus.Voided)
                return false;

            return true;
        }

        public bool CanRefund(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //refund cannot be made if previously a partial refund has been already done. only other partial refund can be made in this case
            //if (shipment.RefundedAmount > decimal.Zero)
            //    return false;

            //uncomment the lines below in shipment to disallow this operation for cancelled shipments
            //if (shipment.shipmentStatus == shipmentStatus.Cancelled)
            //    return false;

            if (shipment.PaymentStatus == PaymentStatus.Paid &&
                _paymentService.SupportRefund(shipment.PaymentMethodSystemName))
                return true;

            return false;
        }

        public bool CanRefundOffline(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //refund cannot be made if previously a partial refund has been already done. only other partial refund can be made in this case
            if (shipment.RefundedAmount > decimal.Zero)
                return false;

            //uncomment the lines below in order to disallow this operation for cancelled orders
            /*if(shipment.DisbursementStatus == DisbursementStatus.NotDisbursed)
                return false;*/

            if (shipment.PaymentStatus == PaymentStatus.Paid)
                return true;

            return false;
        }

        public bool CanPartiallyRefund(CustomerTransaction shipment, decimal zero)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            //decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            //if (canBeRefunded <= decimal.Zero)
            //    return false;

            //if (amountToRefund > canBeRefunded)
            //    return false;

            if ((shipment.PaymentStatus == PaymentStatus.Paid ||
                 shipment.PaymentStatus == PaymentStatus.PartiallyRefunded) &&
                _paymentService.SupportPartiallyRefund(shipment.PaymentMethodSystemName))
                return true;

            return false;
        }

        public bool CanPartiallyRefundOffline(CustomerTransaction shipment, decimal zero)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            //decimal canBeRefunded = order.OrderTotal - order.RefundedAmount;
            //if (canBeRefunded <= decimal.Zero)
            //    return false;

            //if (amountToRefund > canBeRefunded)
            //    return false;

            if (shipment.PaymentStatus == PaymentStatus.Paid ||
                shipment.PaymentStatus == PaymentStatus.PartiallyRefunded)
                return true;

            return false;
        }

        public bool CanVoid(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            if (shipment.PaymentStatus == PaymentStatus.Authorized &&
                _paymentService.SupportVoid(shipment.PaymentMethodSystemName))
                return true;

            return false;
        }

        public bool CanVoidOffline(CustomerTransaction shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (shipment.Amount == decimal.Zero)
                return false;

            //uncomment the lines below in order to allow this operation for cancelled orders
            //if (order.OrderStatus == OrderStatus.Cancelled)
            //    return false;

            if (shipment.PaymentStatus == PaymentStatus.Authorized)
                return true;

            return false;
        }

        public void MarkShipmentAsPaid(CustomerTransaction order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (!CanMarkShipmentAsPaid(order))
                throw new Exception("You can't mark this shipment as paid");

            order.PaymentStatusId = (int)PaymentStatus.Paid;
            order.PaidDateUtc = DateTime.UtcNow;
            _shipmentService.Update(order);

            //add a note
            //order.OrderNotes.Add(new OrderNote
            //{
            //    Note = "Order has been marked as paid",
            //    DisplayToCustomer = false,
            //    CreatedOnUtc = DateTime.UtcNow
            //});
            //_orderService.UpdateOrder(order);

            CheckShipmentStatus(order);

            if (order.PaymentStatus == PaymentStatus.Paid)
            {
                ProcessShipmentAsPaid(order);
            }
        }

        public void ChangeStatus(CustomerTransaction order)
        {
           _shipmentService.Update(order);
        }

        public void DeliverShipment(CustomerTransaction shipment)
        {
           
            
        }

        public void RefundOffline(CustomerTransaction customerTransaction)
        {
            if (customerTransaction == null)
                throw new ArgumentNullException("customerTransaction");

            if (!CanRefundOffline(customerTransaction))
                throw new Exception("You can't refund this Transaction");

            //amout to refund
            decimal amountToRefund = customerTransaction.Amount;

            //total amount refunded
            decimal totalAmountRefunded = customerTransaction.RefundedAmount + amountToRefund;

            //update order info
            customerTransaction.RefundedAmount = totalAmountRefunded;
            customerTransaction.PaymentStatus = PaymentStatus.Refunded;
            customerTransaction.DisbursementStatus = DisbursementStatus.Resolved;
            _shipmentService.Update(customerTransaction);

            
        }

        private void ProcessShipmentAsPaid(CustomerTransaction order)
        {
            
        }

        private void CheckShipmentStatus(CustomerTransaction order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (order.PaymentStatus == PaymentStatus.Paid && !order.PaidDateUtc.HasValue)
            {
                //ensure that paid date is set
                order.PaidDateUtc = DateTime.UtcNow;
                _shipmentService.Update(order);
            }
        }
    }
}