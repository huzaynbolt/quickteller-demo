﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.entities;
using alfred.data.Infrastructure;
using alfred.data.Repository;

namespace alfred.service.CustomerTransactions
{
    public class CustomerTransactionService : ICustomerTransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICustomerTransactionRepository _shipmentRepository;
        
        private readonly IRecurringPaymentRepository _recurringPaymentRepository;
        public CustomerTransactionService(IUnitOfWork unitOfWork,
            ICustomerTransactionRepository shipmentRepository,
            
            IRecurringPaymentRepository recurringPaymentRepository)
        {
            _unitOfWork = unitOfWork;
            _shipmentRepository = shipmentRepository;
            
            _recurringPaymentRepository = recurringPaymentRepository;
        }





        public CustomerTransaction GetById(long id)
        {
            return id <= 0 ? null : _shipmentRepository.GetById(id);
        }

        public CustomerTransaction GetByCustomTrackingNumber(string customTrackingNumber)
        {
            return string.IsNullOrEmpty(customTrackingNumber) ? null : _shipmentRepository.Get(x => x.TrackingNumber == customTrackingNumber);
        }

        public IList<CustomerTransaction> Get(long[] ids)
        {
            if (ids == null || ids.Length == 0)
                return new List<CustomerTransaction>();

            var query = from o in _shipmentRepository.GetAll()
                        where ids.Contains(o.Id) && !o.Deleted
                        select o;
            var shipments = query.ToList();
            //sort by passed identifiers
            return ids.Select(id => shipments.Find(x => x.Id == id)).Where(order => order != null).ToList();
        }

        public CustomerTransaction GetByGuid(Guid shipmentGuid)
        {
            return null;
            //return shipmentGuid == Guid.Empty ? null : _shipmentRepository.Get(x => x. == shipmentGuid);
        }

        public void Delete(CustomerTransaction shipment)
        {
            _shipmentRepository.Delete(shipment);
            Save();
        }

        public IPagedList<CustomerTransaction> Search(long customerId = 0L, string paymentMethodSystemName = null, DateTime? createdFromUtc = null, DateTime? createdToUtc = null, List<int> psIds = null, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _shipmentRepository.GetAll();

            if (customerId > 0)
                query = query.Where(o => o.CustomerId == customerId);


            if (!String.IsNullOrEmpty(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);

            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.PaymentStatusId));

          

            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedResult<CustomerTransaction>(query.AsQueryable(), pageIndex, pageSize);
        }

        public void Insert(CustomerTransaction shipment)
        {
            _shipmentRepository.Add(shipment);
            Save();
        }

        public void Update(CustomerTransaction shipment)
        {
            _shipmentRepository.Update(shipment);
            Save();
        }

        public CustomerTransaction GetByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId,
            string paymentMethodSystemName)
        {
            var query = _shipmentRepository.GetAll();
            if (!String.IsNullOrWhiteSpace(authorizationTransactionId))
                query = query.Where(o => o.AuthorizationTransactionId == authorizationTransactionId);

            if (!String.IsNullOrWhiteSpace(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);

            query = query.OrderByDescending(o => o.CreatedOnUtc);
            var order = query.FirstOrDefault();
            return order;
        }

        public IPagedList<CustomerTransaction> Find(long institutionId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> psIds = null, List<int> dsIds = null, string paymentRef = null, int pageIndex = 0, int pageSize = Int32.MaxValue)
        {
            var query = _shipmentRepository.GetAll();

            if (institutionId > 0)
                query = query.Where(o => o.Customer.OrganizationId == institutionId);


            if (!String.IsNullOrEmpty(paymentRef))
                query = query.Where(o => o.TrackingNumber == paymentRef);

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);

            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.PaymentStatusId));

            if (dsIds != null && dsIds.Any())
                query = query.Where(o => dsIds.Contains(o.StatusId));


            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedResult<CustomerTransaction>(query.AsQueryable(), pageIndex, pageSize);
        }

        public void InsertRecurringPayment(RecurringPayment rp)
        {
           _recurringPaymentRepository.Add(rp);
            Save();
        }

        public void UpdateRecurringPayment(RecurringPayment rp)
        {
            _recurringPaymentRepository.Update(rp);
            Save();
        }

        public IList<RecurringPayment> GetPendingRecurringPayments()
        {
            var now = DateTime.Now;

            var query = from t in _recurringPaymentRepository.GetAll()
                where t.NextRunDate.HasValue && t.NextRunDate <= now && t.IsActive
                orderby t.NextRunDate
                select t;

            return query.ToList();
        }

        public RecurringPayment GetPendingRecurringPaymentById(long id)
        {
            return id <= 0 ? null : _recurringPaymentRepository.GetById(id);
        }

        public IPagedList<RecurringPayment> SearchRecurringPayments(
            long institutionId = 0,

            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int pageIndex = 0, int pageSize = Int32.MaxValue, bool showHidden = true)
        {
            var query = _recurringPaymentRepository.GetAll();

            if (institutionId > 0)
                query = query.Where(o => o.InitialCustomerTransaction.Customer.OrganizationId == institutionId);


           

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            query = query.Where(rp =>
                (showHidden || rp.IsActive));
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedResult<RecurringPayment>(query.AsQueryable(), pageIndex, pageSize);
        }

        private void Save()
        {
            _unitOfWork.Commit();
        }
    }
}