﻿using System.Collections.Generic;
using alfred.data.domain.enums;
using alfred.data.domain.Models;

namespace alfred.service.Security
{
    public interface IUserRoleProvider
    {
        IEnumerable<SystemRole> GetSystemRoles();
        IEnumerable<DefaultUserRole> GetDefaultUserRoles();

        IEnumerable<string> ProductOwnerRoles { get;}

        
    }
}