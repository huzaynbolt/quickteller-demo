﻿using System.Collections.Generic;
using alfred.data.domain.entities;
using alfred.data.domain.Models;

namespace alfred.service.Security
{
    public class StandardPermissionProvider : IPermissionProvider
    {
        //admin area permissions
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin area", SystemName = "AccessAdminPanel", Category = "Standard" };
        public static readonly PermissionRecord ManageSystemLog = new PermissionRecord { Name = "Admin area. Manage System Log", SystemName = "ManageSystemLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageActivityLog = new PermissionRecord { Name = "Admin area. Manage Activity Log", SystemName = "ManageActivityLog", Category = "Configuration" };
        public static readonly PermissionRecord ManageAcl = new PermissionRecord { Name = "Admin area. Manage ACL", SystemName = "ManageACL", Category = "Configuration" };
        public static readonly PermissionRecord ManageUsers = new PermissionRecord { Name = "Admin area. Manage User Accounts", SystemName = "ManageUsers", Category = "Configuration" };
        public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin area. Manage Notifications", SystemName = "ManageNotificationsSettings", Category = "Configuration" };
        public static readonly PermissionRecord CanViewFee = new PermissionRecord { Name = "Can View VggFee", SystemName = "CanViewVggFee", Category = "Configuration" };
        public static readonly PermissionRecord AccessWebService = new PermissionRecord { Name = "Access Web Service", SystemName = "AccessWebService", Category = "Configuration" };
        public static readonly PermissionRecord ManageOrganization = new PermissionRecord() { Name = "Can Manage Organization", SystemName = "ManageOrganization", Category = "Configuration" };
        public static readonly PermissionRecord ManageScheduleTasks = new PermissionRecord { Name = "Admin area. Manage Schedule Tasks", SystemName = "ManageScheduleTasks", Category = "Configuration" };
        public static readonly PermissionRecord CanViewDashBoard = new PermissionRecord() { Name = "Can View Summary DashBoard", SystemName = "CanViewDashBoard", Category = "Configuration" };
        public static readonly PermissionRecord CanViewWallet = new PermissionRecord() { Name = "Can View Wallet", SystemName = "CanViewWallet", Category = "Configuration" };
        public static readonly PermissionRecord ManageUserRoles = new PermissionRecord() { Name = "Manage User Roles", SystemName = "ManageUserRoles", Category = "Configuration" };
        public static readonly PermissionRecord CanManageCustomers = new PermissionRecord() { Name = "Manage Customers", SystemName = "ManageCustomers", Category = "Configuration" };
        public static readonly PermissionRecord CanManageEventHandler = new PermissionRecord(){Name = "Manage Event Handelers", SystemName = "ManageEventHandelers", Category = "Configuration" };
        public static readonly PermissionRecord CanManageCriteria = new PermissionRecord(){Name = "Manage Criteria", SystemName = "ManageCriteria", Category = "Configuration" };
        public static readonly PermissionRecord CanViewTransaction = new PermissionRecord() { Name = "Can View Transaction", SystemName = "CanViewTransaction", Category = "Configuration" };

        public IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[] 
            {
                AccessAdminPanel,
                ManageSystemLog,
                ManageActivityLog,
                ManageAcl,
                ManageUsers,
                ManageSettings,
                CanViewFee,
                AccessWebService,
                ManageOrganization,
                ManageScheduleTasks,
                CanManageCustomers,
                CanViewDashBoard,
                CanViewWallet,
                ManageUserRoles,
                CanManageEventHandler,
                CanManageCriteria,
                CanViewTransaction
                 };
        }

        public IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[] 
            {
                new DefaultPermissionRecord 
                {
                    UserRoleSystemName = SystemUserRoleNames.Root,
                    PermissionRecords = new[] 
                    {     
                     AccessAdminPanel,
                ManageSystemLog,
                ManageActivityLog,
                ManageAcl,
                ManageUsers,
                ManageSettings,
                CanManageCustomers,
                CanViewFee,
                      ManageOrganization,
                     ManageScheduleTasks,
                CanViewDashBoard,
                ManageUserRoles,
                        CanManageEventHandler,
                        CanManageCriteria,
                        CanViewTransaction
                    }
                },
               
                new DefaultPermissionRecord()
                {
                    UserRoleSystemName = SystemUserRoleNames.SegmentOwner,
                    PermissionRecords = new []
                    {
                        CanViewWallet,
                        CanManageCustomers
                    }
                },
                 new DefaultPermissionRecord()
                {
                    UserRoleSystemName = SystemUserRoleNames.ProductOwner,
                    PermissionRecords = new []
                    {
                        CanViewWallet,
                        CanManageCustomers,
                        ManageUsers
                    }
                }
            };
        }

        
    }
}