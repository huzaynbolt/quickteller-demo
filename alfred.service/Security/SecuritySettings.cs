﻿using System.Collections.Generic;
using alfred.service.Configurations;

namespace alfred.service.Security
{
    public class SecuritySettings:ISettings
    {
        public bool ForceSslForAllPages { get; set; }

        /// <summary>
        /// Gets or sets an encryption key
        /// </summary>
        public string EncryptionKey { get; set; }

        /// <summary>
        /// Gets or sets a list of adminn area allowed IP addresses
        /// </summary>
        public List<string> AdminAreaAllowedIpAddresses { get; set; }   
    }
}