﻿using System;
using System.Collections.Generic;
using System.Linq;
using alfred.data.domain.enums;
using alfred.data.domain.Models;

namespace alfred.service.Security
{
    public class StandardUserRoleProvider : IUserRoleProvider
    {
        public IEnumerable<SystemRole> GetSystemRoles()
        {
            var roles = Enum.GetValues(typeof(SystemRole));
            return roles.Cast<SystemRole>().ToList();
        }

        public IEnumerable<DefaultUserRole> GetDefaultUserRoles()
        {
            return new[]
            {
                new DefaultUserRole()
                {
                    SystemName = SystemUserRoleNames.Root,
                    SystemRoles = GetSystemRoles()
                },
                new DefaultUserRole()
                {
                    SystemName = SystemUserRoleNames.ProductOwner,
                    SystemRoles = GetSystemRoles().Except(new[] {SystemRole.Root})
                },
                new DefaultUserRole()
                {
                    SystemName = SystemUserRoleNames.SegmentOwner,
                    SystemRoles = new List<SystemRole>()
                },

            };
        }

        public IEnumerable<string> ProductOwnerRoles
        {
            get { return new List<string>() {SystemUserRoleNames.SegmentOwner}; }
        }

        public IEnumerable<string> RootRoles
        {
            get { return new List<string>() {SystemUserRoleNames.Root}; }
        }
    }
}