﻿namespace alfred.service.Security
{
    public class SystemUserRoleNames
    {
       
        public static string Root { get { return "Root"; } }

        public static string ProductOwner { get { return "ProductOwner"; }}
        public static string BackgroundTask { get { return "BackgroundTask"; } }

       
        public static string SegmentOwner { get { return "SegmentOwner"; } }
    }
}