﻿using alfred.data.domain.entities;

namespace alfred.service.Infrastructure
{
    public interface IOrganizationContext
    {
        /// <summary>
        /// Gets or sets the current organization
        /// </summary>
        Organization CurrentOrganization { get; }
    }
}