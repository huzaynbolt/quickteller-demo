﻿using alfred.data.domain.entities;

namespace alfred.service.Infrastructure
{
    public interface IWorkContext
    {
        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        UserInformation CurrentUser { get; set; }
        /// <summary>
        /// Gets or sets the current institution(logged-in manager)
        /// </summary>
        Organization Organization { get; }

        /// <summary>
        /// Get or set value indicating whether we're in admin area
        /// </summary>
        bool IsAdmin { get; set; }
    }
}
