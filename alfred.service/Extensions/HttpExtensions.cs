﻿using System;
using System.Collections;
using System.Web;

namespace alfred.service.Extensions
{
    public static class HttpExtensions
    {
        // ReSharper disable once InconsistentNaming
         private const string HTTP_CLUSTER_VAR = "HTTP_CLUSTER_HTTPS";
        
        /// <summary>
        /// Gets a value which indicates whether the HTTP connection uses secure sockets (HTTPS protocol). 
        /// Works with Cloud's load balancers.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool IsSecureConnection(this HttpRequestBase request)
        {
            if (!request.IsSslTerminated())
            {
                return request.IsSecureConnection || request.ServerVariables[HTTP_CLUSTER_VAR] != null ||
                       request.ServerVariables[HTTP_CLUSTER_VAR] == "on";
            }    
            var header = request.Headers["X-Forwarded-Proto"];
            return string.Equals(header, "https", StringComparison.OrdinalIgnoreCase);
        }


		
		

		/// <summary>
		/// Gets a value which indicates whether the HTTP connection uses secure sockets (HTTPS protocol). 
		/// Works with Cloud's load balancers.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public static bool IsSecureConnection(this HttpRequest request)
        {
            return request.IsSecureConnection || (request.ServerVariables[HTTP_CLUSTER_VAR] != null || request.ServerVariables[HTTP_CLUSTER_VAR] == "on");
        }

        


        public static bool IsSslTerminated(this HttpRequestBase request)
        {
            return ((IList) request.Headers.AllKeys).Contains("X-Forwarded-Proto");
        }

		
    }
    
}