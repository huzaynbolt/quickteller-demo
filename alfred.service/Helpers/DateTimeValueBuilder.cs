﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace alfred.service.Helpers
{
    public class DateTimeValueBuilder : IQueryValueBuilder
    {
        private readonly string _dateFormat = "MM-dd-yyyy";
        private readonly string[] _formats = {"MM-dd-yyyy", "MM/dd/yyyy" };

        internal static readonly string ValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
        public static readonly Regex IsNumeric = new Regex("(" + ValidIntegerPattern + ")", RegexOptions.Compiled);
        public string GetValue(string value)
        {
            string moment;
            DateTime date;
            if (IsNumeric.IsMatch(value))
            {
                int result;
                moment = int.TryParse(value, out result) ? DateTime.Now.AddDays(result).ToString(_dateFormat) : DateTime.Now.AddDays(result).ToString(_dateFormat);
            }
            else if (DateTime.TryParseExact(value, _formats ,new CultureInfo("en-US"), DateTimeStyles.None, out  date ))
            {
                moment = date.ToString(_dateFormat);
            }
            else
            {
                date = DateTime.Now;
                moment = date.ToString(_dateFormat);
            }


            return moment;
        }
    }
}