﻿using System;

namespace alfred.service.Helpers
{
    public class StringValueBuilder : IQueryValueBuilder
    {
        public string GetValue(string value)
        {
            if (value == "null" || String.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value;
        }
    }
}