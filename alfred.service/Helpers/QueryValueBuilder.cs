﻿using alfred.service.Collections;

namespace alfred.service.Helpers
{
    public class QueryValueBuilder
    {
        private static readonly QueryString QueryString = new QueryString();
        private const char SpitChar = ':';

        public static QueryString BuildFromString(string queryparameter)
        {

            var queryStringParameters = new QueryString();

            var param = QueryString.FillFromString(queryparameter);
            foreach (var pair in param)
            {
                var keyType = pair.ToString().Split(SpitChar);

                if (keyType.Length != 2) continue;
                var type = keyType[1];
                var t = QueryValueBuilderFactory.QueryValueBuilder(type);
                var value = param.Get(pair.ToString());
                value = t == null ? value : t.GetValue(value);
                queryStringParameters.Add(keyType[0], value);
            }
            return queryStringParameters;
        }

    }
}