﻿namespace alfred.service.Helpers
{
    public interface IQueryValueBuilder
    {
        string GetValue(string value);
    }
}