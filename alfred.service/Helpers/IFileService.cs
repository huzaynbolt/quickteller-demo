﻿using System.IO;
using System.Threading.Tasks;

namespace alfred.service.Helpers
{
    public interface IFileService
    {
        Task<T> GetAsync<T>(string id) where T : Stream, new();
        Task InsertOrUpdateAsync(string id, string contentType, Stream stream);
        Task<bool> DeleteIfExistsAsync(string id);
    }
}