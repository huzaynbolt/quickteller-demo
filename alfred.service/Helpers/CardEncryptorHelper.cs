﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;

namespace alfred.service.Helpers
{
    public class CardEncryptorHelper
    {
        public static string PadEncryptionKey(string encryptionKey, string padWith)
        {
            encryptionKey = padWith + encryptionKey + padWith;

            return encryptionKey;
        }


        public static string UnPadEncryptionKey(string encryptionKey, string paddedWith)
        {
            encryptionKey = encryptionKey.TrimStart(paddedWith.ToCharArray()).TrimEnd(paddedWith.ToCharArray());

            return encryptionKey;
        }

        public static byte[] generateKey()
        {
            RandomNumberGenerator rng = RNGCryptoServiceProvider.Create();
            SecureRandom sr = new SecureRandom();
            KeyGenerationParameters kgp = new KeyGenerationParameters(sr, DesEdeParameters.DesEdeKeyLength * 8);
            DesEdeKeyGenerator kg = new DesEdeKeyGenerator();
            byte[] key = new byte[16];

            kg.Init(kgp);
            var keyTmp = kg.GenerateKey();
            int len = key.Length;
            DesEdeParameters.SetOddParity(keyTmp);


            Array.Copy(keyTmp, 0, key, 0, 16);

            return key;
        }


        public static string GetEncryptionKey()
        {
            return Encoding.Default.GetString(Hex.Encode(generateKey()));
        }


        public static CardEncryptedDetails EncryptCreditCardDetails(string pan, string cvv2, string expiry, string pin,
           string encryptionKey)
        {
            var cardEncryptionDetails = new CardEncryptedDetails();

            string dataToBeEncrypted = $"{pan};{cvv2};{expiry};{pin}";


            byte[] inputBytes = Encoding.UTF8.GetBytes(dataToBeEncrypted);
            byte[] iv = new byte[16];
            //for the sake of demo



            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher
                cipher = new PaddedBufferedBlockCipher(blockCipher); //Default scheme is PKCS5/PKCS7
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(encryptionKey));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);



            // Encrypt
            cipher.Init(true, keyParamWithIV);
            byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
            int length = cipher.ProcessBytes(inputBytes, outputBytes, 0);
            cipher.DoFinal(outputBytes, length); //Do the final block
            string encryptedInput = Convert.ToBase64String(outputBytes);

            cardEncryptionDetails.EncryptedData = encryptedInput;
            cardEncryptionDetails.EncryptionKey = encryptionKey;
            cardEncryptionDetails.ChunkedEncryptedData = ChunkEncryptedDetails(encryptedInput);

            return cardEncryptionDetails;

        }



        public static string[] ChunkEncryptedDetails(string encryptedDetails)
        {
            int strLen = encryptedDetails.Length;

            int chunksOf = strLen / 4;

            var chunkedArray = new string[4];

            int startIndex = 0;

            for (int i = 0; i < 4; i++)
            {
                if (i < 3)
                {
                    var chunk = encryptedDetails.Substring(startIndex, chunksOf);
                    chunkedArray[i] = chunk;

                }
                else
                {
                    var chunk = encryptedDetails.Substring(startIndex, strLen - startIndex);
                    chunkedArray[i] = chunk;
                }

                startIndex = startIndex + chunksOf;

            }

            return chunkedArray;

        }

        public static string DecryptCreditCardDetails(string encryptedDetail, string encryptionKey)
        {


            byte[] iv = new byte[16];
         



            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher
                cipher = new PaddedBufferedBlockCipher(blockCipher); //Default scheme is PKCS5/PKCS7
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(encryptionKey));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);


            byte[] outputBytes = Convert.FromBase64String(encryptedDetail);


            //Decrypt            
            cipher.Init(false, keyParamWithIV);
            byte[] comparisonBytes = new byte[cipher.GetOutputSize(outputBytes.Length)];
            int length = cipher.ProcessBytes(outputBytes, comparisonBytes, 0);
            cipher.DoFinal(comparisonBytes, length); //Do the final block

            var decryptedKey = Encoding.UTF8.GetString(comparisonBytes);

            decryptedKey = decryptedKey.TrimEnd('\0');
            return decryptedKey;



        }


    }



    public class CardEncryptedDetails
    {

        public CardEncryptedDetails()
        {
            ChunkedEncryptedData = new string[4];
        }

        public string EncryptionKey { get; set; }

        public string[] ChunkedEncryptedData { get; set; }

        public string EncryptedData { get; set; }
    }
}
