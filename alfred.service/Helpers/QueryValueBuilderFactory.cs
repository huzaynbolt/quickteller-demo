﻿using System;
using System.Collections.Generic;

namespace alfred.service.Helpers
{
    public class QueryValueBuilderFactory
    {
        private static readonly IDictionary<string, Func<IQueryValueBuilder>> Factories = new Dictionary<string, Func<IQueryValueBuilder>>
        {
            {"datetime", () => new DateTimeValueBuilder()},
            {"string", () => new StringValueBuilder()}   
        };




        public static IQueryValueBuilder QueryValueBuilder(string type)
        {
            try
            {
                var factory = Factories[type];
                var builder = factory == null ? null : factory();
                return builder;
            }
            catch (System.Exception)
            {
                // ignored
            }
            return null;
        }






    }
}