﻿using System.IO;
using alfred.data.domain.entities;

namespace alfred.service.Helpers
{
    public interface IImportManager
    {
        void ImportCustomersFromXlsx(Organization inputStream, Stream fileInputStream);
    }
}