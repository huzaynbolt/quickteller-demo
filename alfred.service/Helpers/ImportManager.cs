﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using OfficeOpenXml;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using alfred.data.domain.entities;
using alfred.service.Common;
using alfred.service.Customers;

namespace alfred.service.Helpers
{
    public class ImportManager : IImportManager
    {
        private readonly ICustomerService _customerService;
        private readonly ICustomerAttributeService _customerAttributeService;

        private readonly string _dateFormat = "MM-dd-yyyy";
        private readonly string[] _formats = {"dd/MM/yyyy", "M/dd/yyyy","MM-dd-yyyy", "MM/dd/yyyy", "dd-MM-yyyy"};

        public ImportManager(ICustomerService customerService,
            ICustomerAttributeService customerAttributeService)
        {
            _customerService = customerService;
            _customerAttributeService = customerAttributeService;
        }

        public void ImportCustomersFromXlsx(Organization organization, Stream stream)
        {
            using (var xlPackage = new ExcelPackage(stream))
            {
                var worksheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                    throw new Exception("No worksheet found");
                var row = worksheet.Dimension.End.Row;
                var column = worksheet.Dimension.End.Column;
                var columnNames = new List<string>();
                const int firstRow = 1;
                if (column < 12)
                {
                    throw  new Exception("Invalid Excel Document");
                }
                //Read File Header
                for (var i = 1; i <= column; i++)
                {
                    var worksheetCell = worksheet.Cells[firstRow, i];
                    columnNames.Add(worksheetCell.GetValue<string>());
                }

                //Read Data
                for (var i = 2; i <= row; i++)
                {
                    ExcelRange worksheetCell;
                    try
                    {

                        DateTime anchorDate;
                        DateTime dobDateTime;
                        //ID
                        worksheetCell = worksheet.Cells[i, 1];
                        var id = worksheetCell.GetValue<string>();

                        //First Name
                        worksheetCell = worksheet.Cells[i, 2];
                        var firstName = worksheetCell.GetValue<string>();

                        //LastName
                        worksheetCell = worksheet.Cells[i, 3];
                        var lastName = worksheetCell.GetValue<string>();

                        //Email
                        worksheetCell = worksheet.Cells[i, 4];
                        var email = worksheetCell.GetValue<string>();


                        //Gender
                        worksheetCell = worksheet.Cells[i, 5];
                        var gender = worksheetCell.GetValue<string>();


                        //Address
                        worksheetCell = worksheet.Cells[i, 6];
                        var address = worksheetCell.GetValue<string>();

                        //Area
                        worksheetCell = worksheet.Cells[i, 7];
                        var area = worksheetCell.GetValue<string>();

                        //City
                        worksheetCell = worksheet.Cells[i, 8];
                        var city = worksheetCell.GetValue<string>();

                        //State
                        worksheetCell = worksheet.Cells[i, 9];
                        var state = worksheetCell.GetValue<string>();

                        //DOB
                        worksheetCell = worksheet.Cells[i, 10];
                        var dateOfBirth = worksheetCell.GetValue<string>();
                        DateTime.TryParseExact(dateOfBirth, _formats, new CultureInfo("en-US"), DateTimeStyles.None,
                            out dobDateTime);

                        //AnchorTime
                        worksheetCell = worksheet.Cells[i, 11];
                        var anchorTime = worksheetCell.GetValue<string>();
                        DateTime.TryParseExact(anchorTime, _formats, new CultureInfo("en-US"), DateTimeStyles.None,
                            out anchorDate);


                        //AnchorValue
                        Decimal anchorVal = 0m;
                        worksheetCell = worksheet.Cells[i, 12];
                        var anchorValue = worksheetCell.GetValue<string>();
                        Decimal.TryParse(anchorValue, out anchorVal);

                        var customer = new Customer()
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            Address = address,
                            AnchorTime = anchorDate,
                            AnchorValue = anchorVal,
                            Area = area,
                            City = city,
                            DateOfBirth = dobDateTime,
                            Email = email,
                            PhoneNumber = id,
                            OrganizationId = organization.Id,
                            State = state,
                            CustomerId = id
                        };

                        _customerService.Insert(customer);
                        for (var j = column - 3; j < column; j++)
                        {
                            var columnName = columnNames[j];
                            worksheetCell = worksheet.Cells[i, j + 1];
                            var value = worksheetCell.GetValue<string>();
                            _customerAttributeService.SaveAttribute(customer, columnName, value);
                        }
                    }
                    catch (DbUpdateException e )
                    {
                        /*
                        worksheetCell = worksheet.Cells[i, 1];
                        var id = worksheetCell.GetValue<string>();
                         var  customer = _customerService.GetById(id);
                        var customerAttributeService =
                            DependencyResolver.Current.GetService<ICustomerAttributeService>();
                        if (customer != null)
                        {
                            for (var j = column - 3; j < column; j++)
                            {
                                var columnName = columnNames[j];
                                worksheetCell = worksheet.Cells[i, j + 1];
                                var value = worksheetCell.GetValue<string>();
                                var customerAttribute = new CustomerAttribute()
                                {
                                    EntityId = customer.Id,
                                    Key = columnName,
                                   Value = value,
                                    KeyGroup = "Customer"
                                };
                                customerAttributeService.InsertAttribute(customerAttribute);
                            }    
                        }
                        */
                    }
                    catch (Exception e)
                    {
                        //ignore
                    }
                }
            }
        }
    }
}