﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace alfred.service.Helpers
{
    public interface IUrlShortenerService
    {

        Task<string> Get(string url);
    }

    public class GoogleUrlShortenerService : IUrlShortenerService
    {
        public readonly string Apikey;
        

        public GoogleUrlShortenerService(string apikey)
        {
            Apikey = apikey;
            
        }

        public async Task <string> Get(string url)
        {
            try
            {
               
                var payload = new GoogleUrlShortenerRequest()
                {
                    longUrl = url
                };
                var serializedMessage = JsonConvert.SerializeObject(payload);
              var endpoint = string.Format("https://www.googleapis.com/urlshortener/v1/url?key={0}", Apikey);

                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage(HttpMethod.Post, endpoint)
                    {
                        Content = new StringContent(serializedMessage, Encoding.UTF8, "application/json")
                    };

                  var result =  await client.SendAsync(request);
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        var content = await result.Content.ReadAsStringAsync();

                        var reponse = JsonConvert.DeserializeObject<GoogleUrlShortenerReponse>(content);

                        return reponse.id;
                    }
                }
                
            }
            catch (Exception)
            {
               //
            }

            return url;
        }
    }

    public class GoogleUrlShortenerReponse
    {
        public string kind { get; set; }
        public string id { get; set; }
        public string longUrl { get; set; }
    }


    public class GoogleUrlShortenerRequest
    {
        public string longUrl { get; set; }
    }
}