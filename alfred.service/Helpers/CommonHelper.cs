using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using alfred.service.Infrastructure.ComponentModel;
using PhoneNumbers;

namespace alfred.service.Helpers
{
    public class CommonHelper
    {
        private static bool? _isDevEnvironment;
        /// <summary>
        /// Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                return false;

            email = email.Trim();
            var result = Regex.IsMatch(email, "^(?:[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+\\.)*[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!\\.)){0,61}[a-zA-Z0-9]?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\\[(?:(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\]))$", RegexOptions.IgnoreCase);
            return result;
        }

        /// <summary>
        /// Generate random digit code
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Result string</returns>
        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString());
            return str;
        }

        /// <summary>
        /// Returns an random interger number within a specified rage
        /// </summary>
        /// <param name="min">Minimum number</param>
        /// <param name="max">Maximum number</param>
        /// <returns>Result</returns>
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }

        /// <summary>
        /// Ensure that a string doesn't exceed maximum allowed length
        /// </summary>
        /// <param name="str">Input string</param>
        /// <param name="maxLength">Maximum length</param>
        /// <param name="postfix">A string to add to the end if the original string was shorten</param>
        /// <returns>Input string if its lengh is OK; otherwise, truncated input string</returns>
        public static string EnsureMaximumLength(string str, int maxLength, string postfix = null)
        {
            if (String.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
            {
                var result = str.Substring(0, maxLength);
                if (!String.IsNullOrEmpty(postfix))
                {
                    result += postfix;
                }
                return result;
            }

            return str;
        }

        /// <summary>
        /// Ensures that a string only contains numeric values
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Input string with only numeric values, empty string if input is null/empty</returns>
        public static string EnsureNumericOnly(string str)
        {
            if (String.IsNullOrEmpty(str))
                return string.Empty;

            var result = new StringBuilder();
            foreach (char c in str)
            {
                if (Char.IsDigit(c))
                    result.Append(c);
            }
            return result.ToString();
        }

        /// <summary>
        /// Ensure that a string is not null
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Result</returns>
        public static string EnsureNotNull(string str)
        {
            if (str == null)
                return string.Empty;

            return str;
        }

        /// <summary>
        /// Indicates whether the specified strings are null or empty strings
        /// </summary>
        /// <param name="stringsToValidate">Array of strings to validate</param>
        /// <returns>Boolean</returns>
        public static bool AreNullOrEmpty(params string[] stringsToValidate)
        {
            bool result = false;
            Array.ForEach(stringsToValidate, str =>
            {
                if (string.IsNullOrEmpty(str)) result = true;
            });
            return result;
        }

        public static string Base64ForUrlEncode(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return  string.Empty;
            }
            var encbuff = Encoding.UTF8.GetBytes(str);
            return HttpServerUtility.UrlTokenEncode(encbuff);
        }

        public static string Base64ForUrlDecode(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var decbuff = HttpServerUtility.UrlTokenDecode(str);
                if (decbuff != null) return Encoding.UTF8.GetString(decbuff);
            }
            return string.Empty;
        }


       
        public static bool HasValue(string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        public static int[] ToIntArray(string s)
        {
            return Array.ConvertAll(SplitSafe(s,","), v => int.Parse(v.Trim()));
        }

        public static string[] SplitSafe(string value, string separator)
        {
            if (string.IsNullOrEmpty(value))
                return new string[0];

            // do not use separator.IsEmpty() here because whitespace like " " is a valid separator.
            // an empty separator "" returns array with value.
            if (separator == null)
            {
                separator = "|";

                if (value.IndexOf(separator, StringComparison.Ordinal) < 0)
                {
                    if (value.IndexOf(';') > -1)
                    {
                        separator = ";";
                    }
                    else if (value.IndexOf(',') > -1)
                    {
                        separator = ",";
                    }
                    else if (value.IndexOf(Environment.NewLine, StringComparison.Ordinal) > -1)
                    {
                        separator = Environment.NewLine;
                    }
                }
            }

            return value.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static bool IsWebUrl(string value)
        {
            Regex isWebUrl = new Regex(@"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_\=~]*)?$", RegexOptions.Singleline | RegexOptions.Compiled);
            return !String.IsNullOrEmpty(value) && isWebUrl.IsMatch(value.Trim());
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <returns>The converted value.</returns>
        public static T To<T>(object value)
        {
            //return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            return (T)To(value, typeof(T));
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType)
        {
            return To(value, destinationType, CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <param name="culture">Culture</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType, CultureInfo culture)
        {
            if (value != null)
            {
                var sourceType = value.GetType();

                TypeConverter destinationConverter = GetCustomTypeConverter(destinationType);
                TypeConverter sourceConverter = GetCustomTypeConverter(sourceType);
                if (destinationConverter != null && destinationConverter.CanConvertFrom(value.GetType()))
                    return destinationConverter.ConvertFrom(null, culture, value);
                if (sourceConverter != null && sourceConverter.CanConvertTo(destinationType))
                    return sourceConverter.ConvertTo(null, culture, value, destinationType);
                if (destinationType.IsEnum && value is int)
                    return Enum.ToObject(destinationType, (int)value);
                if (!destinationType.IsInstanceOfType(value))
                    return Convert.ChangeType(value, destinationType, culture);
            }
            return value;
        }

        public static TypeConverter GetCustomTypeConverter(Type type)
        {
            if (type == typeof(List<int>))
                return new GenericListTypeConverter<int>();
            if (type == typeof(List<decimal>))
                return new GenericListTypeConverter<decimal>();
            if (type == typeof(List<string>))
                return new GenericListTypeConverter<string>();

            return TypeDescriptor.GetConverter(type);
        }


        public static string ArrayToString(int[] array, string seperator = ",")
        {
            if (array == null || array.Length <= 0)
            {
                return string.Empty;
            }
            return string.Join(seperator, array);
        }


        public static string RegexRemove(string input, string pattern, RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Multiline)
        {
            return Regex.Replace(input, pattern, string.Empty, options);
        }

        public static string RegexReplace(string input, string pattern, string replacement, RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Multiline)
        {
            return Regex.Replace(input, pattern, replacement, options);
        }

        public static string ProperName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }

            string properName;
            var temp = name.Trim().Split(' ');
            if (temp.Length > 0)
            {
                temp = temp.Where(word => word.Length > 0).ToArray();
                var words = temp.Select(word => string.Concat(word.Substring(0, 1).ToUpper(), word.Substring(1).ToLower())).ToArray();
                properName = string.Join(" ", words);
            }
            else
            {
                properName = string.Concat(name.Substring(0, 1).ToUpper(), name.Substring(1).ToLower());
            }

            return properName;
        }

        public static string ParseString(Dictionary<string, string> placeHolders, string body)
        {
            return placeHolders.Keys.Where(key => Regex.IsMatch(key, @"\${[^""]+}")).Aggregate(body, (current, key) => current.Replace(key, placeHolders[key]));
        }

        /// <summary>
        /// Gets a setting from the application's <c>web.config</c> <c>appSettings</c> node
        /// </summary>
        /// <typeparam name="T">The type to convert the setting value to</typeparam>
        /// <param name="key">The key of the setting</param>
        /// <param name="defValue">The default value to return if the setting does not exist</param>
        /// <returns>The casted setting value</returns>
        public static T GetAppSetting<T>(string key, T defValue = default(T))
        {
            if (key == null) throw new ArgumentNullException("key");

            var setting = ConfigurationManager.AppSettings[key];

            if (setting == null)
            {
                return defValue;
            }

            return To<T>(setting);
        }

        public static bool HasConnectionString(string connectionStringName)
        {
            var conString = ConfigurationManager.ConnectionStrings[connectionStringName];
            if (conString != null && HasValue(conString.ConnectionString))
            {
                return true;
            }

            return false;
        }


        public static bool IsDevEnvironment
        {
            get
            {
                if (!_isDevEnvironment.HasValue)
                {
                    _isDevEnvironment = IsDevEnvironmentInternal();
                }

                return _isDevEnvironment.Value;
            }
        }

        private static bool IsDevEnvironmentInternal()
        {
            if (!HostingEnvironment.IsHosted)
                return true;

            if (HostingEnvironment.IsDevelopmentEnvironment)
                return true;

            if (Debugger.IsAttached)
                return true;

            // if there's a 'SmartStore.NET.sln' in one of the parent folders,
            // then we're likely in a dev environment
            if (FindSolutionRoot(HostingEnvironment.MapPath("~/")) != null)
                return true;

            return false;
        }

        private static DirectoryInfo FindSolutionRoot(string currentDir)
        {
            var dir = Directory.GetParent(currentDir);
            while (true)
            {
                if (dir == null || IsSolutionRoot(dir))
                    break;

                dir = dir.Parent;
            }

            return dir;
        }

        private static bool IsSolutionRoot(DirectoryInfo dir)
        {
            return File.Exists(Path.Combine(dir.FullName, "VATCollect.sln"));
        }

        [DebuggerStepThrough]
        public static string EnsureStartsWith(string value, string startsWith)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (startsWith == null) throw new ArgumentNullException("startsWith");
           

            return value.StartsWith(startsWith) ? value : (startsWith + value);
        }

        /// <summary>
        /// Ensures the target string ends with the specified string.
        /// </summary>
        /// <param name="endWith">The target.</param>
        /// <param name="value">The value.</param>
        /// <returns>The target string with the value string at the end.</returns>
        [DebuggerStepThrough]
        public static string EnsureEndsWith(string value, string endWith)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (endWith == null) throw new ArgumentNullException("endWith");


            if (value.Length >= endWith.Length)
            {
                if (string.Compare(value, value.Length - endWith.Length, endWith, 0, endWith.Length, StringComparison.OrdinalIgnoreCase) == 0)
                    return value;

                string trimmedString = value.TrimEnd(null);

                if (string.Compare(trimmedString, trimmedString.Length - endWith.Length, endWith, 0, endWith.Length, StringComparison.OrdinalIgnoreCase) == 0)
                    return value;
            }

            return value + endWith;
        }


        public static bool IsValidPhoneNumber(string phoneNumber, string countryCode = "NG")
        {
            bool valid = false;

            try
            {
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.GetInstance();

                var number = phoneNumberUtil.Parse(phoneNumber, countryCode);
                valid = phoneNumberUtil.IsPossibleNumber(number);
            }
            catch (NumberParseException)
            {

            }
            catch (Exception)
            {
                //
            }

            return valid;
        }

        public static string FormatPhoneNumber(string phoneNumber, string countryCode = "NG", bool includePlusSign = false)
        {
            try
            {
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.GetInstance();
                if (!IsValidPhoneNumber(phoneNumber)) return string.Empty;
                var number = phoneNumberUtil.Parse(phoneNumber, countryCode);
                var phone = phoneNumberUtil.Format(number, PhoneNumberFormat.E164);
                if (includePlusSign)
                {
                    return phone;
                }
                return phone.StartsWith("+") ? phone.Substring(1, phone.Length - 1) : phone;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {


            }
            return phoneNumber;
        }


        public static string TripleDESEncrypt(string source, string key)
        {
            var desCryptoProvider = new TripleDESCryptoServiceProvider();
            var hashMd5Provider = new MD5CryptoServiceProvider();

            var byteHash = hashMd5Provider.ComputeHash(Encoding.UTF8.GetBytes(key));
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB
            var byteBuff = Encoding.UTF8.GetBytes(source);

            var encoded =
                Convert.ToBase64String(desCryptoProvider.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            return encoded;
        }


        public static string TripleDESDecrypt(string encodedText, string key)
        {
            var desCryptoProvider = new TripleDESCryptoServiceProvider();
            var hashMd5Provider = new MD5CryptoServiceProvider();

            var byteHash = hashMd5Provider.ComputeHash(Encoding.UTF8.GetBytes(key));
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB
            var byteBuff = Convert.FromBase64String(encodedText);

            var plaintext = Encoding.UTF8.GetString(desCryptoProvider.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            return plaintext;
        }

        public static string CreateKey(int size = 32)
        {
            string key;
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                byte[] secretKeyByteArray = new byte[size]; //256 bit
                cryptoProvider.GetBytes(secretKeyByteArray);
                key = Convert.ToBase64String(secretKeyByteArray);
            }
            return key;
        }

        public static string EncryptSHA512(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            using (SHA512 sha512 = SHA512.Create())
            {
                byte[] bytesSHA512In = Encoding.UTF8.GetBytes(text);
                byte[] bytesSHA512Out = sha512.ComputeHash(bytesSHA512In);
                string strSHA512Out = BitConverter.ToString(bytesSHA512Out);
                strSHA512Out = strSHA512Out.Replace("-", "");
                return strSHA512Out;
            }
        }
    }
}