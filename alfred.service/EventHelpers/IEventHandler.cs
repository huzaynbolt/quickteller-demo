﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace alfred.service.EventHelpers
{
    public interface IEventHandler
    {
        string SystemName { get; }
        string FriendlyName { get; }
        Task Handle(Dictionary<string, string> parameters);
    }
}