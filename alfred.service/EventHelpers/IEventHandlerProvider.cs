﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace alfred.service.EventHelpers
{
    public interface IEventHandlerProvider
    {
        IEventHandler GetEventHandler(string systemName);
        IList<IEventHandler> GetEventHandlers();
    }

  public  class EventHandlerProvider : IEventHandlerProvider
    {
        
        private readonly IDictionary<string, Func<IEventHandler>> _factories;

        public EventHandlerProvider(IDictionary<string, Func<IEventHandler>> factories)
        {
            _factories = factories;
        }


        
        public IEventHandler GetEventHandler(string systemName)
        {
            var factory = _factories[systemName];
            var handler = factory == null ? null : factory();
            return handler;
        }

        public IList<IEventHandler> GetEventHandlers()
        {
            return _factories.Values.Select(f => f()).ToList();
        }
    }
}